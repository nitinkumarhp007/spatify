package com.spatify.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "Authorization-Key";
    public static final String AUTH_KEY = "auth_key";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String EMAIL = "email";
    public static final String FROM_EMAIL = "from_email";
    public static final String SOCIAL_ID = "soical_id";
    public static final String SOCIAL_TYPE = "login_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "device_type";
    public static final String RANDOM_NUMBER = "random_number";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String COUNTRY_CODE = "country_code";
    public static final String COUNTRY = "country";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String FIRST_NAME ="first_name" ;
    public static final String LAST_NAME ="last_name" ;
    public static final String PROFILE = "profile";
    public static final String PHONE_CODE ="phone_code" ;
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String LOCATIONS = "locations";
    public static final String MEDIA = "media";
    public static final String TYPE = "type";
    public static final String THUMB = "thumb";
    public static final String COMMENT = "comment";
    public static final String POST_ID = "post_id";

    public static final String FRIEND_ID ="friend_id" ;
    public static final String IMAGE ="image" ;
    public static final String CATEGORY_ID ="category_id" ;
    public static final String USER_ID ="user_id" ;
    public static final String PROVIDER_ID = "provider_id";
    public static final String STATUS = "status";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String RATING = "rating";
    public static final String REVIEW = "review";
    public static final String CALL_COUNT ="call_count" ;
    public static final String SMS_COUNT ="sms_count" ;
    public static final String IS_FEATURED = "is_featured";
    public static final String RESPONCE_TYPE = "response_type";
    public static final String SERVICE = "service";
    public static final String FACILITY = "facility";
    public static final String SEARCH = "search";
    public static final String GENDER = "gender";
    public static final String RADIUS = "radius";
    public static final String AREA = "area";
    public static final String MIN_PRICE="min_price" ;
    public static final String MAX_PRICE = "min_price";
    public static final String IS_NEARBY = "is_nearby";
}
















