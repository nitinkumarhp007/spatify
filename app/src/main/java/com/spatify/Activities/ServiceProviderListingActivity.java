package com.spatify.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.spatify.Adapters.ServiceProvoderlistAdapter;
import com.spatify.DataModel.FacilityServiceModel;
import com.spatify.DataModel.ProviderModel;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.GPSTracker;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class ServiceProviderListingActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    public ServiceProviderListingActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.sort)
    TextView sort;
    @BindView(R.id.filter)
    TextView filter;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.sort_layout)
    RelativeLayout sortLayout;
    @BindView(R.id.filter_layout)
    RelativeLayout filterLayout;
    @BindView(R.id.open_now)
    TextView openNow;
    private SavePref savePref;
    String category_id = "";
    String category_name = "";
    String service = "";
    String facility = "";
    String latitude = "";
    String longitude = "";
    String gender = "";
    String country = "";
    String search = "";
    String rating = "";
    String response_type = "";
    String location = "";
    String area = "";
    String min_cost = "";
    String max_cost = "";
    String is_nearby = "0";
    ArrayList<ProviderModel> list;
    ArrayList<ProviderModel> list_open;
    ServiceProvoderlistAdapter adapter;

    boolean is_open = false;

    GoogleApiClient googleApiClient;
    FusedLocationProviderClient fusedLocationClient = null;

    public boolean is_filter = false;
    ArrayList<FacilityServiceModel> facility_list = new ArrayList<>();
    ArrayList<FacilityServiceModel> services_list = new ArrayList<>();
    public boolean from_home_filter = false;

    GPSTracker gpsTracker = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_provider_listing);
        ButterKnife.bind(this);

        context = ServiceProviderListingActivity.this;
        savePref = new SavePref(context);
        gpsTracker = new GPSTracker(context);


        from_home_filter = getIntent().getBooleanExtra("from_home_filter", false);

        if (from_home_filter) {
            category_id = getIntent().getStringExtra("category_id");
            facility_list = getIntent().getParcelableArrayListExtra("facility_list");
            services_list = getIntent().getParcelableArrayListExtra("services_list");

            service = getIntent().getStringExtra("service");
            facility = getIntent().getStringExtra("facility");
            // latitude = getIntent().getStringExtra("latitude");
            //longitude = getIntent().getStringExtra("longitude");
            gender = getIntent().getStringExtra("gender");
            country = getIntent().getStringExtra("country");
            search = getIntent().getStringExtra("search");
            rating = getIntent().getStringExtra("rating");
            location = getIntent().getStringExtra("location");
            min_cost = getIntent().getStringExtra("min_cost");
            max_cost = getIntent().getStringExtra("max_cost");

            category_name = getIntent().getStringExtra("category_name");
            area = getIntent().getStringExtra("area");

            is_filter = true;
            setToolbar();
/*
            if (ConnectivityReceiver.isConnected()) {
                FILTER_PROVIDER_LISTING(response_type);
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }*/

            /*  if (!gpsTracker.canGetLocation())*/
            // EnableGPSAutoMatically();


            if (ConnectivityReceiver.isConnected()) {
                FILTER_PROVIDER_LISTING(response_type);
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }


        } else {
            category_id = getIntent().getStringExtra("category_id");
            category_name = getIntent().getStringExtra("category_name");

            if (gpsTracker.canGetLocation()) {


            } else {
                // EnableGPSAutoMatically();
            }


            if (ConnectivityReceiver.isConnected()) {
                FILTER_PROVIDER_LISTING("1");
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }
        }
        setToolbar();


    }

    @Override
    protected void onResume() {
        super.onResume();

        latitude = String.valueOf(gpsTracker.getLatitude());
        longitude = String.valueOf(gpsTracker.getLongitude());

    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
                            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    Activity#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for Activity#requestPermissions for more details.
                                return;
                            }
                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());


                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(context, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }


                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                // util.showToast(getActivity(), "lat long " + location.getLatitude());


                /*gpsTracker = new GPSTracker(context);
                //util.showToast(context, "latt " + String.valueOf(gpsTracker.getLatitude()));
                if (ConnectivityReceiver.isConnected()) {
                    AGENCIE("", true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }*/

                latitude = String.valueOf(location.getLatitude());
                longitude = String.valueOf(location.getLongitude());


                if (ConnectivityReceiver.isConnected())
                    FILTER_PROVIDER_LISTING(response_type);
                else
                    util.IOSDialog(context, util.internet_Connection_Error);


                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };

    private void FILTER_PROVIDER_LISTING(String response_type) {

        Log.e("latlong_g", latitude + "  " + longitude);

        String country_string = country;
        /*if (!country.isEmpty()) {
            country = country.toLowerCase().replace(" ", "");
            country_string = country.substring(0, 1).toUpperCase() + country.substring(1).toLowerCase();
        }*/


        myRecyclerView.setVisibility(View.GONE);
        if (category_id.equals("")) {
            category_id = "0";
        }

        //   Log.e("location", latitude + " " + longitude + " " + is_nearby);
        Log.e("location", country_string);

        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);// 1 for spa ,2= massuer
        formBuilder.addFormDataPart(Parameters.USER_ID, savePref.getID());
        formBuilder.addFormDataPart(Parameters.SERVICE, service);
        formBuilder.addFormDataPart(Parameters.IS_FEATURED, "");
        formBuilder.addFormDataPart(Parameters.FACILITY, facility);
        formBuilder.addFormDataPart(Parameters.SEARCH, search);
        formBuilder.addFormDataPart(Parameters.RESPONCE_TYPE, response_type);
        Log.e("response_type", response_type);
        formBuilder.addFormDataPart(Parameters.RATING, rating);
        if (response_type.equals("5")) {
            Log.e("Locationp", latitude + " " + longitude);
            formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
            formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);

            // formBuilder.addFormDataPart(Parameters.LATITUDE, "1.277240");
            //formBuilder.addFormDataPart(Parameters.LONGITUDE, "103.830223");
        }

        formBuilder.addFormDataPart(Parameters.GENDER, gender);
        formBuilder.addFormDataPart(Parameters.COUNTRY, country_string);
        formBuilder.addFormDataPart(Parameters.AREA, area);
        formBuilder.addFormDataPart(Parameters.IS_NEARBY, is_nearby);
        formBuilder.addFormDataPart(Parameters.MIN_PRICE, min_cost);
        formBuilder.addFormDataPart(Parameters.MAX_PRICE, max_cost);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FILTER_PROVIDER_LISTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                list_open = new ArrayList<>();
                if (list_open.size() > 0)
                    list_open.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i).getJSONObject("Provider");
                                ProviderModel providerModel = new ProviderModel();
                                providerModel.setId(object.getString("id"));
                                providerModel.setFirst_name(object.getString("first_name"));
                                providerModel.setView(object.getString("view"));
                                if (object.getString("category_name").equals("Spa")) {
                                    providerModel.setId_show("S0" + object.getString("id_show"));
                                } else {
                                    providerModel.setId_show("M0" + object.getString("id_show"));
                                }
                                providerModel.setIs_open(object.getString("is_open"));
                                providerModel.setReview(object.getString("review"));
                                providerModel.setRating(object.getString("rating"));
                                providerModel.setIs_like(object.optString("is_like"));
                                if (body.getJSONObject(i).getJSONArray("Image").length() > 0)
                                    providerModel.setImage(body.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                list.add(providerModel);

                                if (object.getString("is_open").equals("1"))
                                    list_open.add(providerModel);
                            }

                            if (is_open)
                                list = list_open;

                            if (list.size() > 0) {
                                adapter = new ServiceProvoderlistAdapter(context, list);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                                myRecyclerView.setAdapter(adapter);

                                // savePref.setStringLatest("last_pos", String.valueOf(position));

                                if (!savePref.getStringLatest("last_pos").isEmpty()) {
                                    myRecyclerView.scrollToPosition(Integer.parseInt(savePref.getStringLatest("last_pos")));
                                    savePref.setStringLatest("last_pos", "");
                                }

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorText.setVisibility(View.VISIBLE);
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void PROVIDER_LISTING(String response_type) {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);// 1 for spa ,2= massuer
        formBuilder.addFormDataPart(Parameters.USER_ID, savePref.getID());
        formBuilder.addFormDataPart(Parameters.RESPONCE_TYPE, response_type);
        formBuilder.addFormDataPart(Parameters.IS_FEATURED, "0");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FEATURED_PROVIDER_LISTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i).getJSONObject("Provider");
                                ProviderModel providerModel = new ProviderModel();
                                providerModel.setId(object.getString("id"));
                                providerModel.setFirst_name(object.getString("first_name"));
                                providerModel.setView(object.getString("view"));
                                if (object.getString("category_name").equals("Spa")) {
                                    providerModel.setId_show("S0" + object.getString("id_show"));
                                } else {
                                    providerModel.setId_show("M0" + object.getString("id_show"));
                                }
                                providerModel.setIs_open(object.getString("is_open"));
                                providerModel.setReview(object.getString("review"));
                                providerModel.setRating(object.getString("rating"));
                                providerModel.setIs_like(object.optString("is_like"));
                                if (body.getJSONObject(i).getJSONArray("Image").length() > 0)
                                    providerModel.setImage(body.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                list.add(providerModel);
                            }
                            if (list.size() > 0) {
                                adapter = new ServiceProvoderlistAdapter(context, list);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                                myRecyclerView.setAdapter(adapter);

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorText.setVisibility(View.VISIBLE);
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DO_LIKE_API(int position) {
        String status = "";
        if (list.get(position).getIs_like().equals("1")) {
            status = "0";
        } else {
            status = "1";
        }
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, list.get(position).getId());
        formBuilder.addFormDataPart(Parameters.STATUS, status);
        RequestBody formBody = formBuilder.build();
        String finalStatus = status;
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.DO_LIKE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            list.get(position).setIs_like(finalStatus);
                            adapter.notifyDataSetChanged();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(category_name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.open_now, R.id.sort_layout, R.id.filter_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.open_now:
                is_open = true;
                if (list_open.size() > 0) {
                    adapter = new ServiceProvoderlistAdapter(context, list_open);
                    myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                    myRecyclerView.setAdapter(adapter);
                    myRecyclerView.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.GONE);
                } else {
                    myRecyclerView.setVisibility(View.GONE);
                    errorText.setVisibility(View.VISIBLE);
                    errorText.setText("No open providers found");
                }
                break;
            case R.id.sort_layout:
                startActivityForResult(new Intent(context, SortActivity.class), 666);
                break;
            case R.id.filter_layout:
                Intent intent = new Intent(this, FilterActivity.class);
                intent.putExtra("category_id", category_id);
                intent.putExtra("facility_list", facility_list);
                intent.putExtra("services_list", services_list);
                intent.putExtra("service", service);
                intent.putExtra("facility", facility);
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                intent.putExtra("location", location);
                intent.putExtra("gender", gender);
                intent.putExtra("country", country);
                intent.putExtra("search", search);
                intent.putExtra("rating", rating);
                intent.putExtra("area", area);
                intent.putExtra("min_cost", min_cost);
                intent.putExtra("max_cost", max_cost);
                startActivityForResult(intent, 200);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 200) {
            category_id = data.getStringExtra("category_id");
            service = data.getStringExtra("service");
            facility = data.getStringExtra("facility");
            //latitude = data.getStringExtra("latitude");
            //longitude = data.getStringExtra("longitude");
            gender = data.getStringExtra("gender");
            country = data.getStringExtra("country");
            search = data.getStringExtra("search");
            rating = data.getStringExtra("rating");
            location = data.getStringExtra("location");
            min_cost = data.getStringExtra("min_cost");
            max_cost = data.getStringExtra("max_cost");
            facility_list = data.getParcelableArrayListExtra("facility_list");
            services_list = data.getParcelableArrayListExtra("services_list");

            category_name = data.getStringExtra("category_name");
            area = data.getStringExtra("area");

            is_filter = true;
            setToolbar();

            if (ConnectivityReceiver.isConnected()) {
                FILTER_PROVIDER_LISTING(response_type);
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }


        } else if (resultCode == RESULT_OK && requestCode == 400) {
           /* is_filter = data.getBooleanExtra("is_filter", false);
            if (ConnectivityReceiver.isConnected())
                if (!is_filter) {
                    Log.e("hitttt", "1");
                    FILTER_PROVIDER_LISTING(response_type);
                } else {
                    Log.e("hitttt", "2");
                    FILTER_PROVIDER_LISTING(response_type);
                }
            else
                util.IOSDialog(context, util.internet_Connection_Error);*/

        } else if (resultCode == RESULT_OK && requestCode == 666) {
            response_type = data.getStringExtra("response_type");
            sort.setText(data.getStringExtra("name"));

            if (response_type.equals("5")) {
                is_nearby = "1";

               /* if (gpsTracker.canGetLocation()) {
                    if (ConnectivityReceiver.isConnected())
                        FILTER_PROVIDER_LISTING(response_type);
                    else
                        util.IOSDialog(context, util.internet_Connection_Error);
                } else {
                    EnableGPSAutoMatically();
                }*/

            } else {
                is_nearby = "0";
            }

            if (ConnectivityReceiver.isConnected())
                FILTER_PROVIDER_LISTING(response_type);
            else
                util.IOSDialog(context, util.internet_Connection_Error);


        }
    }


    private void Show_Sorting() {
        final Item[] items = {
                new Item("Latest Post", R.drawable.heart_r),
                new Item("Oldest Post", android.R.drawable.ic_menu_gallery),
                new Item("Most Views", android.R.drawable.ic_notification_clear_all),
                new Item("Reviews Rating", android.R.drawable.ic_notification_clear_all),
                new Item("Nearest", android.R.drawable.ic_notification_clear_all),
        };
        ListAdapter adapter = new ArrayAdapter(
                context,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setTextSize(14f);
                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };


        new AlertDialog.Builder(context)
                .setTitle("Sort")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            sort.setText("Latest Post");
                        } else if (item == 1) {
                            sort.setText("Oldest Post");
                        } else if (item == 2) {
                            sort.setText("Most Views");
                        } else if (item == 3) {
                            sort.setText("Reviews Rating");
                        } else if (item == 4) {
                            sort.setText("Nearest");

                        }

                        response_type = String.valueOf(item + 1);
                        dialog.dismiss();

                        if (ConnectivityReceiver.isConnected())
                            if (!is_filter) {
                                FILTER_PROVIDER_LISTING(response_type);
                            } else {
                                FILTER_PROVIDER_LISTING(response_type);
                            }

                        else
                            util.IOSDialog(context, util.internet_Connection_Error);
                    }
                }).show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public static class Item {
        public final String text;
        //  public final int icon;

        public Item(String text, Integer icon) {
            this.text = text;
            //this.icon = icon;
        }

        @Override
        public String toString() {
            return text;
        }
    }
}
