package com.spatify.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.spatify.Adapters.AreaAdapter;
import com.spatify.DataModel.AreaModel;
import com.spatify.R;
import com.spatify.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AreaListActivity extends AppCompatActivity {
    @BindView(R.id.title)
    ImageView title;
    @BindView(R.id.done)
    TextView done;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    //ArrayList<AreaModel> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_list);
        ButterKnife.bind(this);


        Log.e("size", String.valueOf(util.list.length));
        Log.e("size", String.valueOf(util.list_check.length));


        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter(new AreaAdapter(this, util.list, util.list_check));


    }

    @OnClick({R.id.title, R.id.done})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title:
                finish();
                break;
            case R.id.done:

                String areas = "";
                String areas_api = "";

                for (int i = 0; i < util.list_check.length; i++) {

                    if (util.list_check[i]) {
                        if (areas.isEmpty()) {
                            areas = util.list[i];
                            areas_api = util.list[i];
                        } else {
                            areas = areas + " , " + util.list[i];
                            areas_api = areas_api + "|" + util.list[i];
                        }
                    }

                }


                Intent intent = new Intent();
                intent.putExtra("area", areas);
                intent.putExtra("areas_api", areas_api);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }
}
