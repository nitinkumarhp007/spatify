package com.spatify.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.spatify.Adapters.DetailProvidersAdapter;
import com.spatify.Adapters.IImageViewPagerAdapter;
import com.spatify.Adapters.ImageViewPagerAdapter;
import com.spatify.Adapters.ServiceFacilitiesAdapter;
import com.spatify.Adapters.ServicesAdapter;
import com.spatify.DataModel.PackageModel;
import com.spatify.DataModel.ProviderModel;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.GPSTracker;
import com.spatify.Util.GpsUtils;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddDetailActivity extends AppCompatActivity {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.fav_button)
    ImageView favButton;
    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;
    @BindView(R.id.review)
    LinearLayout review;
    @BindView(R.id.call)
    Button call;
    @BindView(R.id.sms)
    Button sms;
    @BindView(R.id.back_button)
    ImageView back_button;
    @BindView(R.id.directions)
    Button directions;
    @BindView(R.id.my_recycler_view_nearby_spa)
    RecyclerView myRecyclerViewNearbySpa;
    @BindView(R.id.my_recycler_view_nearby_masseur)
    RecyclerView myRecyclerViewNearbyMasseur;
    @BindView(R.id.my_recycler_view_available)
    RecyclerView myRecyclerViewAvailable;
    @BindView(R.id.opening_time_top)
    LinearLayout openingTimeTop;
    @BindView(R.id.opening_time_layout)
    LinearLayout openingTimeLayout;
    @BindView(R.id.gender)
    TextView gender;
    @BindView(R.id.country)
    TextView country;
    @BindView(R.id.claim_report_this_profile)
    TextView claimReportThisProfile;
    @BindView(R.id.share)
    ImageView share;
    @BindView(R.id.id)
    TextView id_;
    @BindView(R.id.view)
    TextView view_;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.category_name)
    TextView categoryName;
    @BindView(R.id.rating_text)
    TextView ratingText;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.rating_count)
    TextView ratingCount;
    @BindView(R.id.featured)
    TextView featured;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.email_address)
    TextView emailAddress;
    @BindView(R.id.website)
    TextView website;
    @BindView(R.id.opening_time)
    TextView openingTime;
    @BindView(R.id.monday)
    TextView monday;
    @BindView(R.id.tuesday)
    TextView tuesday;
    @BindView(R.id.wednesday)
    TextView wednesday;
    @BindView(R.id.thursday)
    TextView thursday;
    @BindView(R.id.friday)
    TextView friday;
    @BindView(R.id.saturday)
    TextView saturday;
    @BindView(R.id.review_count__)
    TextView review_count__;
    @BindView(R.id.sunday)
    TextView sunday;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.nearby_list)
    TextView nearbyList;
    @BindView(R.id.featured_list)
    TextView featuredList;
    @BindView(R.id.available_list)
    TextView availableList;
    @BindView(R.id.facilities_rv)
    RecyclerView facilitiesRv;
    @BindView(R.id.services_rv)
    RecyclerView servicesRv;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.gender_layout)
    LinearLayout gender_layout;
    @BindView(R.id.email_layout)
    LinearLayout email_layout;
    @BindView(R.id.country_layout)
    LinearLayout country_layout;
    @BindView(R.id.spa_belong_layout)
    LinearLayout spa_belong_layout;
    @BindView(R.id.spa_belong_text)
    TextView spa_belong_text;
    @BindView(R.id.image_spa)
    ImageView image_spa;
    @BindView(R.id.id__spa)
    TextView id__spa;
    @BindView(R.id.view_spa)
    TextView view_spa;
    @BindView(R.id.review_spa)
    TextView review_spa;
    @BindView(R.id.name_spa)
    TextView name_spa;
    @BindView(R.id.facilities_layout)
    LinearLayout facilities_layout;
    @BindView(R.id.phone_layput)
    LinearLayout phone_layput;
    @BindView(R.id.additional_phone_layput)
    LinearLayout additional_phone_layput;
    @BindView(R.id.services_layout)
    LinearLayout services_layout;
    @BindView(R.id.website_layout)
    LinearLayout website_layout;
    @BindView(R.id.additional_phone)
    TextView additional_phone;
    @BindView(R.id.date_established)
    TextView date_established;
    @BindView(R.id.price_range)
    TextView price_range;
    @BindView(R.id.open_now)
    TextView openNow;
    @BindView(R.id.price_range_layout)
    LinearLayout price_range_layout;
    @BindView(R.id.date_established_layout)
    LinearLayout date_established_layout;

    @BindView(R.id.information)
    LinearLayout information;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.services)
    LinearLayout services;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.my_recycler_view_services)
    RecyclerView my_recycler_view_services;
    @BindView(R.id.info_layout)
    LinearLayout info_layout;
    @BindView(R.id.close_main)
    ImageView close_main;


    private SavePref savePref;
    AddDetailActivity context;
    ArrayList<String> image_list;
    String id = "", is_like = "", latitude = "", longitude = "", phone_text = "", additional_phone_text = "";

    ArrayList<ProviderModel> featured_list;
    ArrayList<ProviderModel> nearby_list;
    ArrayList<ProviderModel> open_providers_list;
    ArrayList<PackageModel> package_list;
    ArrayList<String> facility_list;
    ArrayList<String> services_list;

    String call_count = "", sms_count = "";
    boolean isGPS = false;
    GPSTracker gpsTracker;

    String spa_belong_id = "", category_id = "";
    ArrayList<ProviderModel> list_belong_messeurs;

    boolean is_from_push = false;
    boolean is_deep_link = false;
    Dialog dialog=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_detail);
        ButterKnife.bind(this);

        context = AddDetailActivity.this;
        savePref = new SavePref(context);
        id = getIntent().getStringExtra("id");
        is_from_push = getIntent().getBooleanExtra("is_from_push", false);
        is_deep_link = getIntent().getBooleanExtra("is_deep_link", false);

        gpsTracker = new GPSTracker(context);


        setToolbar();


        openingTimeTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (openingTimeLayout.getVisibility() == View.VISIBLE)
                    openingTimeLayout.setVisibility(View.GONE);
                else
                    openingTimeLayout.setVisibility(View.VISIBLE);
            }
        });


        favButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (savePref.getAuthorization_key().isEmpty()) {
                    util.login_first(context);
                } else {
                    DO_LIKE_API();
                }


            }
        });

        close_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close_main.setVisibility(View.GONE);
                if (close_main.getVisibility() == View.VISIBLE) {
                    if (dialog != null)
                        dialog.dismiss();
                }
            }
        });

        emailAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", emailAddress.getText().toString().trim(), null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, name.getText().toString().trim());
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        });


        website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = website.getText().toString().trim();
                if (!url.startsWith("https://") && !url.startsWith("http://")) {
                    url = "http://" + url;
                }
                Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(openUrlIntent);
            }
        });


        if (is_from_push) {
            if (!getIntent().getStringExtra("image").isEmpty()) {
                Log.e("image___", getIntent().getStringExtra("image"));
                showDialog(context, "", getIntent().getStringExtra("image"));
            } else {
                if (ConnectivityReceiver.isConnected()) {
                    PROVIDER_DETAIL();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        } else {
            /*new GpsUtils(context).turnGPSOn(new GpsUtils.onGpsListener() {
                @Override
                public void gpsStatus(boolean isGPSEnable) {
                    // turn on GPS
                    isGPS = isGPSEnable;
                    if (!isGPS) {
                        //is_network_off = true;
                    } else {

                    }
                    Log.e("callllll", "gpsStatus");
                }
            });*/

            if (ConnectivityReceiver.isConnected()) {
                PROVIDER_DETAIL();
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }
        }


        information.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v1.setBackgroundColor(getResources().getColor(R.color.light_blue));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                info_layout.setVisibility(View.VISIBLE);
                my_recycler_view_services.setVisibility(View.GONE);
            }
        });
        services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v2.setBackgroundColor(getResources().getColor(R.color.light_blue));
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                my_recycler_view_services.setVisibility(View.VISIBLE);
                info_layout.setVisibility(View.GONE);

            }
        });


        transparentStatusAndNavigation();
    }

    private void transparentStatusAndNavigation() {
        //make full transparent statusBar
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            /*getWindow().setNavigationBarColor(Color.TRANSPARENT);*/
        }
    }

    private void setWindowFlag(final int bits, boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void showDialog(Activity activity, String msg, String image) {
        Log.e("image", image);
        dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        RelativeLayout middle = (RelativeLayout) dialog.findViewById(R.id.middle);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        ImageView image_ = (ImageView) dialog.findViewById(R.id.image);
        ProgressBar progress_bar = (ProgressBar) dialog.findViewById(R.id.progress_bar);


        image_.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
               // dialog.dismiss();
                return false;
            }
        });

        Glide.with(context)
                .load(image)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progress_bar.setVisibility(View.GONE);
                        middle.setVisibility(View.GONE);
                        close.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progress_bar.setVisibility(View.GONE);
                        middle.setVisibility(View.VISIBLE);
                        close.setVisibility(View.VISIBLE);
                       close_main.setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .into(image_);

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            PROVIDER_DETAIL();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == util.GPS_REQUEST) {
                Log.e("callllll", "onActivityResult");
                isGPS = true; // flag maintain before get location
            }
        }
    }


    public void PROVIDER_DETAIL() {
        Log.e("location__", String.valueOf(gpsTracker.getLatitude()) + " " + String.valueOf(gpsTracker.getLongitude()));
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, id);
        formBuilder.addFormDataPart(Parameters.USER_ID, savePref.getID());
        //formBuilder.addFormDataPart(Parameters.PROVIDER_ID, "44");

        // formBuilder.addFormDataPart(Parameters.LATITUDE, "1.317600");
        //formBuilder.addFormDataPart(Parameters.LONGITUDE, "103.732070");

        formBuilder.addFormDataPart(Parameters.LATITUDE, String.valueOf(gpsTracker.getLatitude()));
        formBuilder.addFormDataPart(Parameters.LONGITUDE, String.valueOf(gpsTracker.getLongitude()));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.PROVIDER_DETAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {

                Log.e("result_data", result);
                mDialog.dismiss();
                image_list = new ArrayList<>();
                featured_list = new ArrayList<>();
                nearby_list = new ArrayList<>();
                facility_list = new ArrayList<>();
                services_list = new ArrayList<>();
                open_providers_list = new ArrayList<>();
                package_list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonObject.getJSONObject("body");
                            text.setText(body.getString("first_name"));
                            categoryName.setText(body.getString("category_name"));


                            if (is_from_push) {
                                if (!getIntent().getStringExtra("package_id").equals("0")) {
                                    v2.setBackgroundColor(getResources().getColor(R.color.light_blue));
                                    v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                                    my_recycler_view_services.setVisibility(View.VISIBLE);
                                    info_layout.setVisibility(View.GONE);
                                }
                            }

                            category_id = body.getString("category_id");
                            JSONArray Packages = body.getJSONArray("Packages");
                            for (int i = 0; i < Packages.length(); i++) {
                                JSONObject object = Packages.getJSONObject(i).getJSONObject("Package");
                                PackageModel packageModel = new PackageModel();
                                packageModel.setDescription(object.getString("description"));
                                packageModel.setId(object.getString("id"));
                                packageModel.setIs_featured(object.optString("is_featured"));
                                packageModel.setName(object.getString("name"));
                                packageModel.setPrice(object.getString("price"));
                                package_list.add(packageModel);
                            }

                            if (package_list.size() > 0) {
                                my_recycler_view_services.setLayoutManager(new LinearLayoutManager(context));
                                my_recycler_view_services.setAdapter(new ServicesAdapter(context, package_list));
                                services.setVisibility(View.VISIBLE);
                            } else {
                                services.setVisibility(View.INVISIBLE);
                            }

                            if (category_id.equals("1")) {

                                nearbyList.setText("In House Masseur");
                                featuredList.setText("Nearby Spa");
                                availableList.setText("Featured Spa");


                                id_.setText("ID:" + "S" + body.getString("id_show"));
                                //JSONArray open_providers = body.getJSONArray("open_providers");
                                JSONArray Masseurs = body.getJSONArray("Masseurs");
                                for (int i = 0; i < Masseurs.length(); i++) {
                                    JSONObject object = Masseurs.getJSONObject(i).getJSONObject("Provider");
                                    ProviderModel providerModel = new ProviderModel();
                                    providerModel.setId(object.getString("id"));
                                    providerModel.setFirst_name(object.getString("first_name"));
                                    providerModel.setView(object.getString("view"));
                                    if (object.getString("category_name").equals("Spa")) {
                                        providerModel.setId_show("S" + object.getString("id_show"));
                                    } else {
                                        providerModel.setId_show("M" + object.getString("id_show"));
                                    }
                                    providerModel.setIs_open(object.getString("is_open"));
                                    providerModel.setReview(object.getString("review"));
                                    providerModel.setRating(object.getString("rating"));
                                    if (Masseurs.getJSONObject(i).getJSONArray("Image").length() > 0)
                                        providerModel.setImage(Masseurs.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                    open_providers_list.add(providerModel);
                                }
                                if (open_providers_list.size() > 0) {
                                    myRecyclerViewNearbySpa.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    myRecyclerViewNearbySpa.setAdapter(new DetailProvidersAdapter(context, open_providers_list));
                                    myRecyclerViewNearbySpa.setVisibility(View.VISIBLE);
                                    nearbyList.setVisibility(View.VISIBLE);
                                } else {
                                    myRecyclerViewNearbySpa.setVisibility(View.GONE);
                                    nearbyList.setVisibility(View.GONE);
                                }


                                spa_belong_layout.setVisibility(View.GONE);
                                spa_belong_text.setVisibility(View.GONE);
                                id_.setText("ID:" + "S" + body.getString("id_show"));

                                //spa


                                JSONArray featured_providers = body.getJSONArray("featured_providers");
                                for (int i = 0; i < featured_providers.length(); i++) {
                                    JSONObject object = featured_providers.getJSONObject(i).getJSONObject("Provider");
                                    ProviderModel providerModel = new ProviderModel();
                                    providerModel.setId(object.getString("id"));
                                    providerModel.setFirst_name(object.getString("first_name"));
                                    providerModel.setView(object.getString("view"));
                                    providerModel.setIs_open(object.getString("is_open"));
                                    if (object.getString("category_name").equals("Spa")) {
                                        providerModel.setId_show("S" + object.getString("id_show"));
                                    } else {
                                        providerModel.setId_show("M" + object.getString("id_show"));
                                    }
                                    providerModel.setReview(object.getString("review"));
                                    providerModel.setRating(object.getString("rating"));
                                    if (featured_providers.getJSONObject(i).getJSONArray("Image").length() > 0)
                                        providerModel.setImage(featured_providers.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                    featured_list.add(providerModel);
                                }

                                if (featured_list.size() > 0) {
                                    myRecyclerViewAvailable.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    myRecyclerViewAvailable.setAdapter(new DetailProvidersAdapter(context, featured_list));

                                    myRecyclerViewAvailable.setVisibility(View.VISIBLE);
                                    availableList.setVisibility(View.VISIBLE);

                                } else {
                                    myRecyclerViewAvailable.setVisibility(View.GONE);
                                    availableList.setVisibility(View.GONE);
                                }


                                JSONArray nearby_providers = body.getJSONArray("nearby_providers");
                                for (int i = 0; i < nearby_providers.length(); i++) {
                                    JSONObject object = nearby_providers.getJSONObject(i).getJSONObject("Provider");
                                    ProviderModel providerModel = new ProviderModel();
                                    providerModel.setId(object.getString("id"));
                                    if (object.getString("category_name").equals("Spa")) {
                                        providerModel.setId_show("S" + object.getString("id_show"));
                                    } else {
                                        providerModel.setId_show("M" + object.getString("id_show"));
                                    }
                                    providerModel.setFirst_name(object.getString("first_name"));
                                    providerModel.setIs_open(object.getString("is_open"));
                                    providerModel.setView(object.getString("view"));
                                    providerModel.setReview(object.getString("review"));
                                    providerModel.setRating(object.getString("rating"));
                                    if (nearby_providers.getJSONObject(i).getJSONArray("Image").length() > 0)
                                        providerModel.setImage(nearby_providers.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                    nearby_list.add(providerModel);
                                }

                                if (nearby_list.size() > 0) {
                                    myRecyclerViewNearbyMasseur.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    myRecyclerViewNearbyMasseur.setAdapter(new DetailProvidersAdapter(context, nearby_list));

                                    myRecyclerViewNearbyMasseur.setVisibility(View.VISIBLE);
                                    featuredList.setVisibility(View.VISIBLE);
                                } else {
                                    myRecyclerViewNearbyMasseur.setVisibility(View.GONE);
                                    featuredList.setVisibility(View.GONE);
                                }


                            } else {
                                id_.setText("ID:" + "M" + body.getString("id_show"));
                                //masseur
                                if (body.getJSONObject("Spa").length() > 0) {
                                    JSONObject object = body.getJSONObject("Spa").getJSONObject("Provider");
                                    spa_belong_id = object.getString("id");
                                    name_spa.setText(object.getString("first_name"));
                                    id__spa.setText("ID:S" + object.getString("id"));
                                    view_spa.setText(object.getString("view"));
                                    review_spa.setText(object.getString("review"));
                                    Glide.with(context).load(body.getJSONObject("Spa").getJSONArray("Image").getJSONObject(0).getString("image")).into(image_spa);

                                    spa_belong_layout.setVisibility(View.VISIBLE);
                                    spa_belong_text.setVisibility(View.VISIBLE);

                                    if (object.getString("is_open").equals("1")) {
                                        openNow.setVisibility(View.VISIBLE);
                                        openNow.setText("Open Now");
                                        openNow.setTextColor(context.getResources().getColor(R.color.white));
                                    } else {
                                        openNow.setVisibility(View.VISIBLE);
                                        openNow.setText("Close Now");
                                        openNow.setTextColor(context.getResources().getColor(R.color.red));
                                    }


                                } else {
                                    spa_belong_layout.setVisibility(View.GONE);
                                    spa_belong_text.setVisibility(View.GONE);
                                }
                                myRecyclerViewAvailable.setVisibility(View.GONE);
                                availableList.setVisibility(View.GONE);

                                nearbyList.setText("Nearby " + body.getString("category_name"));
                                featuredList.setText("Featured " + body.getString("category_name"));
                                availableList.setText("Available " + "Masseur");


                                JSONArray featured_providers = body.getJSONArray("featured_providers");
                                for (int i = 0; i < featured_providers.length(); i++) {
                                    JSONObject object = featured_providers.getJSONObject(i).getJSONObject("Provider");
                                    ProviderModel providerModel = new ProviderModel();
                                    providerModel.setId(object.getString("id"));
                                    providerModel.setFirst_name(object.getString("first_name"));
                                    providerModel.setView(object.getString("view"));
                                    providerModel.setIs_open(object.getString("is_open"));
                                    if (object.getString("category_name").equals("Spa")) {
                                        providerModel.setId_show("S" + object.getString("id_show"));
                                    } else {
                                        providerModel.setId_show("M" + object.getString("id_show"));
                                    }
                                    providerModel.setReview(object.getString("review"));
                                    providerModel.setRating(object.getString("rating"));
                                    if (featured_providers.getJSONObject(i).getJSONArray("Image").length() > 0)
                                        providerModel.setImage(featured_providers.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                    featured_list.add(providerModel);
                                }

                                if (featured_list.size() > 0) {
                                    myRecyclerViewNearbyMasseur.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    myRecyclerViewNearbyMasseur.setAdapter(new DetailProvidersAdapter(context, featured_list));

                                    myRecyclerViewNearbyMasseur.setVisibility(View.VISIBLE);
                                    featuredList.setVisibility(View.VISIBLE);

                                } else {
                                    myRecyclerViewNearbyMasseur.setVisibility(View.GONE);
                                    featuredList.setVisibility(View.GONE);
                                }

                                JSONArray nearby_providers = body.getJSONArray("nearby_providers");
                                for (int i = 0; i < nearby_providers.length(); i++) {
                                    JSONObject object = nearby_providers.getJSONObject(i).getJSONObject("Provider");
                                    ProviderModel providerModel = new ProviderModel();
                                    providerModel.setId(object.getString("id"));
                                    if (object.getString("category_name").equals("Spa")) {
                                        providerModel.setId_show("S" + object.getString("id_show"));
                                    } else {
                                        providerModel.setId_show("M" + object.getString("id_show"));
                                    }
                                    providerModel.setFirst_name(object.getString("first_name"));
                                    providerModel.setIs_open(object.getString("is_open"));
                                    providerModel.setView(object.getString("view"));
                                    providerModel.setReview(object.getString("review"));
                                    providerModel.setRating(object.getString("rating"));
                                    if (nearby_providers.getJSONObject(i).getJSONArray("Image").length() > 0)
                                        providerModel.setImage(nearby_providers.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                    nearby_list.add(providerModel);
                                }

                                if (nearby_list.size() > 0) {
                                    myRecyclerViewNearbySpa.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                                    myRecyclerViewNearbySpa.setAdapter(new DetailProvidersAdapter(context, nearby_list));

                                    myRecyclerViewNearbySpa.setVisibility(View.VISIBLE);
                                    nearbyList.setVisibility(View.VISIBLE);
                                } else {
                                    myRecyclerViewNearbySpa.setVisibility(View.GONE);
                                    nearbyList.setVisibility(View.GONE);
                                }

                            }


                            if (body.getString("phone").trim().isEmpty()) {
                                phone_layput.setVisibility(View.GONE);
                            } else {
                                phone.setText(body.getString("phone"));
                                phone_layput.setVisibility(View.VISIBLE);
                            }

                            if (body.getString("website").isEmpty()) {
                                website_layout.setVisibility(View.GONE);
                            } else {
                                website_layout.setVisibility(View.VISIBLE);
                                website.setText(body.getString("website"));

                            }
                            address.setText(body.getString("location"));
                            view_.setText(body.getString("view"));
                            review_count__.setText(body.getString("review"));
                            ratingCount.setText("(" + body.getString("review") + ")");
                            ratingBar.setRating(Float.parseFloat(body.getString("rating")));
                            ratingText.setText(body.getString("rating"));

                            if (!body.getString("established").isEmpty()) {
                                date_established.setText(body.getString("established") + " ");
                                date_established_layout.setVisibility(View.VISIBLE);
                            } else {
                                date_established_layout.setVisibility(View.GONE);
                            }
                            if (!body.getString("min_price").equals("0") && !body.getString("max_price").equals("0")) {
                                price_range.setText("$" + body.getString("min_price") + " to " + "$" + body.getString("max_price") + " ");
                                price_range_layout.setVisibility(View.VISIBLE);
                            } else {
                                price_range_layout.setVisibility(View.GONE);
                            }


                            call_count = body.getString("call_count");
                            sms_count = body.getString("sms_count");

                            call.setText("Call (" + body.getString("call_count") + ")");
                            sms.setText("SMS (" + body.getString("sms_count") + ")");

                            monday.setText(body.getString("1_opening") + "-" + body.getString("1_closing"));
                            tuesday.setText(body.getString("2_opening") + "-" + body.getString("2_closing"));
                            wednesday.setText(body.getString("3_opening") + "-" + body.getString("3_closing"));
                            thursday.setText(body.getString("4_opening") + "-" + body.getString("4_closing"));
                            friday.setText(body.getString("5_opening") + "-" + body.getString("5_closing"));
                            saturday.setText(body.getString("6_opening") + "-" + body.getString("6_closing"));
                            sunday.setText(body.getString("7_opening") + "-" + body.getString("7_closing"));


                            if (!body.getString("description").isEmpty()) {
                                description.setText(body.getString("description"));
                                description.setVisibility(View.VISIBLE);
                            } else {
                                description.setVisibility(View.GONE);
                            }

                            if (body.getString("is_featured").equals("1"))
                                featured.setVisibility(View.VISIBLE);
                            else
                                featured.setVisibility(View.INVISIBLE);

                            is_like = body.getString("is_like");
                            if (is_like.equals("1")) {
                                favButton.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_wf));
                            } else {
                                favButton.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_w));
                            }

                            for (int i = 0; i < body.getJSONArray("Images").length(); i++) {
                                JSONObject object = body.getJSONArray("Images").getJSONObject(i).getJSONObject("Image");

                                image_list.add(object.getString("image"));
                            }
                            IImageViewPagerAdapter adapter = new IImageViewPagerAdapter(AddDetailActivity.this, image_list);
                            pager.setAdapter(adapter);
                            pager.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            indicator.setViewPager(pager);


                            latitude = body.getString("latitude");
                            longitude = body.getString("longitude");
                            phone_text = body.getString("phone");
                            additional_phone_text = body.getString("additional_phone");

                            if (additional_phone_text.isEmpty()) {
                                additional_phone_layput.setVisibility(View.GONE);
                            } else {
                                additional_phone_layput.setVisibility(View.VISIBLE);
                                additional_phone.setText(additional_phone_text);
                            }

                            if (body.getString("gender").equals("1")) {
                                gender.setText("Male");
                                gender_layout.setVisibility(View.VISIBLE);
                            } else if (body.getString("gender").equals("2")) {
                                gender.setText("Female");
                                gender_layout.setVisibility(View.VISIBLE);
                            } else {
                                gender_layout.setVisibility(View.GONE);
                            }


                            if (body.getString("country").isEmpty()) {
                                country_layout.setVisibility(View.GONE);
                            } else {
                                country.setText(body.getString("country"));
                                country_layout.setVisibility(View.VISIBLE);
                            }
                            if (body.getString("email").isEmpty()) {
                                email_layout.setVisibility(View.GONE);
                            } else {
                                emailAddress.setText(body.getString("email"));
                                email_layout.setVisibility(View.VISIBLE);
                            }


                            JSONArray Facility = body.getJSONArray("Facility");
                            for (int i = 0; i < Facility.length(); i++) {
                                JSONObject object = Facility.getJSONObject(i).getJSONObject("Facility");
                                facility_list.add(object.getString("name"));
                            }
                            if (facility_list.size() > 0) {
                                facilitiesRv.setLayoutManager(new GridLayoutManager(context, 3));
                                facilitiesRv.setAdapter(new ServiceFacilitiesAdapter(context, facility_list));
                                facilities_layout.setVisibility(View.VISIBLE);
                            } else {
                                facilities_layout.setVisibility(View.GONE);
                            }


                            JSONArray Services = body.getJSONArray("Services");
                            for (int i = 0; i < Services.length(); i++) {
                                JSONObject object = Services.getJSONObject(i).getJSONObject("Service");
                                services_list.add(object.getString("name"));
                            }
                            if (services_list.size() > 0) {
                                servicesRv.setLayoutManager(new GridLayoutManager(context, 3));
                                servicesRv.setAdapter(new ServiceFacilitiesAdapter(context, services_list));

                                services_layout.setVisibility(View.VISIBLE);
                            } else {
                                services_layout.setVisibility(View.GONE);
                            }


                            scrollView.setVisibility(View.VISIBLE);

                        } else {
                            util.IOSDialog(context, jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Detail");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        if (is_from_push || is_deep_link) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            finish();
        } else {
            Intent intent = new Intent();
            intent.putExtra("is_filter", getIntent().getBooleanExtra("is_filter", false));
            setResult(RESULT_OK, intent);
            finish();
        }


    }

    public void DO_LIKE_API() {
        String status = "";
        if (is_like.equals("1")) {
            status = "0";
        } else {
            status = "1";
        }
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, id);
        formBuilder.addFormDataPart(Parameters.STATUS, status);
        RequestBody formBody = formBuilder.build();
        String finalStatus = status;
        String finalStatus1 = status;
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.DO_LIKE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            is_like = finalStatus1;

                            if (is_like.equals("1")) {
                                favButton.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_wf));
                            } else {
                                favButton.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_w));
                            }

                        } else {
                            util.IOSDialog(context, jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick(R.id.review)
    public void onClick() {
        Intent intent = new Intent(this, ReviewListActivity.class);
        intent.putExtra("provider_id", id);
        intent.putExtra("ratingText", ratingText.getText().toString());
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

    @OnClick({R.id.back_button, R.id.spa_belong_layout, R.id.claim_report_this_profile, R.id.share, R.id.call, R.id.sms, R.id.directions})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.spa_belong_layout:
                Intent intent = new Intent(context, AddDetailActivity.class);
                intent.putExtra("id", spa_belong_id);
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.claim_report_this_profile:
                Intent intent1 = new Intent(context, ContactUSActivity.class);
                intent1.putExtra("provider_id", id);
                startActivity(intent1);
                context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.share:
                sharepost(id);
                break;
            case R.id.call:
                if (additional_phone_text.trim().isEmpty()) {
                    SMS_AND_CALL_COUNT_API(true, phone_text);
                } else if (phone_text.trim().isEmpty()) {
                    SMS_AND_CALL_COUNT_API(true, additional_phone_text);
                } else {
                    Show_call();
                }

                break;
            case R.id.sms:
                if (additional_phone_text.isEmpty()) {
                    util.IOSDialog(context, "Not Available");
                } else {
                    SMS_AND_CALL_COUNT_API(false, "");
                }

                break;
            case R.id.directions:
                loadNavigationView(latitude, longitude);
                break;
        }
    }

    private void Show_call() {
        final ServiceProviderListingActivity.Item[] items = {
                new ServiceProviderListingActivity.Item(phone_text, null),
                new ServiceProviderListingActivity.Item(additional_phone_text, null),
        };
        ListAdapter adapter = new ArrayAdapter(
                context,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setTextSize(14f);
                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };


        new androidx.appcompat.app.AlertDialog.Builder(context)
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            SMS_AND_CALL_COUNT_API(true, phone_text);
                        } else if (item == 1) {
                            SMS_AND_CALL_COUNT_API(true, additional_phone_text);
                        }
                    }
                }).show();
    }


    public void SMS_AND_CALL_COUNT_API(boolean is_call, String number) {
        String sms__ = sms_count;

        String call__ = call_count;

        if (is_call)
            call__ = String.valueOf(Integer.parseInt(call__) + 1);
        else
            sms__ = String.valueOf(Integer.parseInt(sms__) + 1);


        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, id);
        formBuilder.addFormDataPart(Parameters.CALL_COUNT, call__);
        formBuilder.addFormDataPart(Parameters.SMS_COUNT, sms__);
        RequestBody formBody = formBuilder.build();
        sms_count = sms__;
        call_count = call__;
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SMS_AND_CALL_COUNT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            if (is_call) {
                                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
                                startActivity(intent);
                            } else {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + additional_phone_text));
                                intent.putExtra("sms_body", "Hi " + text.getText().toString() + ", I saw your profile from Spatify App, can I make an appointment for massage?\n" + "\n" + "嗨 " + text.getText().toString() + "，我在Spatify App中看到了您的个人资料，我可以预约按摩吗? ");
                                startActivity(intent);
                            }

                            call.setText("Call (" + call_count + ")");
                            sms.setText("SMS (" + sms_count + ")");


                        } else {
                            util.IOSDialog(context, jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public void loadNavigationView(String lat, String lng) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("www.google.com")
                .appendPath("maps")
                .appendPath("dir")
                .appendPath("")
                .appendQueryParameter("api", "1")
                .appendQueryParameter("destination", Double.parseDouble(lat) + "," + Double.parseDouble(lng));
        String url = builder.build().toString();
        Log.e("Directions", url);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url + "&mode=driving"));
        startActivity(i);
    }

    private void sharepost(String post_id) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        //  sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://3.22.158.181/spatify/users/open/" + post_id);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "http://www.app.spatify.sg/spatify/users/open/" + post_id);
        context.startActivity(Intent.createChooser(sharingIntent, "Share Profile External"));
    }

}
