package com.spatify.Activities;

import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.spatify.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutUsActivity extends AppCompatActivity {

    @BindView(R.id.term_text)
    TextView termText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);
        setToolbar();

        String text = "<font color=#ffffff>Spatify is a newly created App that is available on both IOS and Android platform which allow users in discovering all Spas, Massage Parlours, Masseurs and their services island-wide within Singapore region." +
                "\n" + "\n" +
                "" +
                "" +
                "In line with the latest trend and technologies, Spatify is here to create a new experience for wellness industry in how consumers select their choices of massage locations, massage therapists and massage services\n" +
                "\n" + "\n" +
                "Spatify aims to firmly set its footprint in the wellness industry. Our main goal: To provide consumers the tools to focus on their well-being and live well all year round.\n" +
                "\n" + "\n" +
                "Interested parties of Spa Owner or Massage Therapist to join our platform, kindly write to us at \"Contact Us\" or add us via </font>";
        String text1 = "<font color=#66ff33><b>Wechat ID: spatify</b></font>";
        String text2 = "<font color=#ffffff> Spatify是一个全新创建的应用程序，可在IOS(苹果)和Android(安卓)平台上使用，目的是允许用户发现新加坡全岛范围内的所有Spa，按摩中心，及不同的按摩师及按摩服务。\n" +
                "\n" + "\n" +
                "为了顺应最新趋势和技术，Spatify为保健行业创造了新的体验，使消费者可以选择自己喜欢的按摩地点，按摩治疗师，和各项·按摩服务\n" +
                "\n" + "\n" +
                "Spatify旨在稳固其在保健行业的足迹。我们的主要目标：为消费者提供全年专注于自己的保健，舒缓身心的应用程序。\n" +
                "\n" + "\n" +
                "有兴趣加入我们平台的按摩院负责人或按摩师可以在“Contact Us”中联系我们或通过微信联络我们。</font>";
        String text3 = "<font color=#66ff33><b>微信号：spatify</b></font>";


        String t = "<p>Spatify is a newly created App that is available on both IOS and Android platform which allow users in discovering all Spas, Massage Parlours, Masseurs and their services island-wide within Singapore region.<br /><br />In line with the latest trend and technologies, Spatify is here to create a new experience for wellness industry in how consumers select their choices of massage locations, massage therapists and massage services<br /><br />Spatify aims to firmly set its footprint in the wellness industry. Our main goal: To provide consumers the tools to focus on their well-being and live well all year round.<br /><br />Interested parties of Spa Owner or Massage Therapist to join our platform, kindly write to us at \"Contact Us\" or add us via <font color=#66ff33><strong>Wechat ID: spatify</strong></font><br /><br />Spatify是一个全新创建的应用程序，可在IOS(苹果)和Android(安卓)平台上使用，目的是允许用户发现新加坡全岛范围内的所有Spa，按摩中心，及不同的按摩师及按摩服务。<br /><br />为了顺应最新趋势和技术，Spatify为保健行业创造了新的体验，使消费者可以选择自己喜欢的按摩地点，按摩治疗师，和各项&middot;按摩服务<br /><br />Spatify旨在稳固其在保健行业的足迹。我们的主要目标：为消费者提供全年专注于自己的保健，舒缓身心的应用程序。<br /><br />有兴趣加入我们平台的按摩院负责人或按摩师可以在&ldquo;Contact Us&rdquo;中联系我们或通过微信联络我们。<br /><font color=#66ff33><strong>微信号：spatify</strong></font></p>";
        // termText.setText(Html.fromHtml(text + text1 + text2 + text3));
        termText.setText(Html.fromHtml(t));


    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("About Us");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
