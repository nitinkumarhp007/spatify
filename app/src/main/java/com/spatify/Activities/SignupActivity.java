package com.spatify.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignupActivity extends AppCompatActivity {
    SignupActivity context;
    @BindView(R.id.password_text)
    TextView passwordText;
    private SavePref savePref;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.term_check)
    CheckBox termCheck;
    @BindView(R.id.sign_up)
    Button signUp;
    @BindView(R.id.sign_in)
    Button signIn;
    Uri fileUri;
    @BindView(R.id.term_check_text)
    TextView termCheckText;
    @BindView(R.id.term_check_lay)
    LinearLayout termCheckLay;
    private String selectedimage = "";
    private boolean is_from_social = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        context = SignupActivity.this;
        savePref = new SavePref(context);

        is_from_social = getIntent().getBooleanExtra("is_from_social", false);


        if (is_from_social) {
            name.setText(getIntent().getStringExtra("name"));
            emailAddress.setText(getIntent().getStringExtra("email"));
            Glide.with(this).load(getIntent().getStringExtra("image")).error(R.drawable.user1).into(profilePic);

            passwordText.setVisibility(View.GONE);
            password.setVisibility(View.GONE);

        }
    }

    private void socialLoginApi() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.SOCIAL_ID, getIntent().getStringExtra("social_id"));
        formBuilder.addFormDataPart(Parameters.IMAGE, getIntent().getStringExtra("image"));
        formBuilder.addFormDataPart(Parameters.SOCIAL_TYPE, getIntent().getStringExtra("social_type"));
        formBuilder.addFormDataPart(Parameters.FIRST_NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SOCIAL_LOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                           /* savePref.setAuthorization_key(body.getString("authorization_key"));
                            savePref.setID(body.getString("id"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setName(body.getString("first_name"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setImage(body.getString("image"));
                            savePref.setStringLatest("is_from_social", "1");
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "Login Sucessfully!!!");*/




                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setMessage("Verification code have been sent to your registered email. Please check your all email folder or spam folder.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                  //  Toast.makeText(SignupActivity.this, "Verification code have been sent to your registered email. Please check your all email folder or spam folder.", Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(context, OTPActivity.class);
                                    try {
                                        intent.putExtra("authorization_key", body.getString("authorization_key"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    intent.putExtra("is_from_social", "1");
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @OnClick({R.id.back_button, R.id.term_check_text, R.id.term_check_lay, R.id.profile_pic, R.id.sign_up, R.id.sign_in})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.profile_pic:
                if (!is_from_social) {
                    int MyVersion = Build.VERSION.SDK_INT;
                    if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                        if (!checkIfAlreadyhavePermission()) {
                            requestForSpecificPermission();
                        }
                        else {
                            CropImage.activity(fileUri)
                                    .setAspectRatio(2, 2)
                                    .setGuidelines(CropImageView.Guidelines.ON)
                                    .start(this);
                        }
                    }
                    else {
                        CropImage.activity(fileUri)
                                .setAspectRatio(2, 2)
                                .setGuidelines(CropImageView.Guidelines.ON)
                                .start(this);
                    }
                }
                break;
            case R.id.sign_up:
                SignUpProcess();
                break;
            case R.id.sign_in:
                startActivity(new Intent(this, SignInActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.term_check_text:
                Intent intent = new Intent(context, PrivacyActivity.class);
                intent.putExtra("privacy", false);
                intent.putExtra("link", "https://spatify.sg/termsofuse/");
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.term_check_lay:
                break;
        }
    }

    private boolean checkIfAlreadyhavePermission() {
        int result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.activity(fileUri)
                            .setAspectRatio(2, 2)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
                } else {
                    //not granted
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }



    private void SignUpProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (is_from_social) {
                if (name.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Your Name");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (emailAddress.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Email Address");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                    util.IOSDialog(context, "Please Enter a Vaild Email Address");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (phone.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Phone");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (phone.getText().toString().length() < 8) {
                    util.IOSDialog(context, "Phone Number should be of 8 digits");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (!termCheck.isChecked()) {
                    util.IOSDialog(context, "Please Accept Terms & Conditions");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else {
                    socialLoginApi();
                }
            } else {

                if (selectedimage.isEmpty()) {
                    util.IOSDialog(context, "Please Select Image");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (name.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Your Name");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (emailAddress.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Email Address");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                    util.IOSDialog(context, "Please Enter a Vaild Email Address");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (password.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Password");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (phone.getText().toString().trim().isEmpty()) {
                    util.IOSDialog(context, "Please Enter Phone");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (phone.getText().toString().length() < 8) {
                    util.IOSDialog(context, "Phone Number should be of 8 digits");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else if (!termCheck.isChecked()) {
                    util.IOSDialog(context, "Please Accept Terms & Conditions");
                    signUp.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                } else {
                    USER_SIGNUP_API();
                }
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    private void USER_SIGNUP_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.FIRST_NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());

        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USER_SIGNUP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);

                          //  Toast.makeText(SignupActivity.this, "", Toast.LENGTH_LONG).show();

                            new IOSDialog.Builder(context)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setCancelable(false)
                                    .setMessage("Verification code have been sent to your registered email. Please check your all email folder or spam folder.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, OTPActivity.class);
                                    try {
                                        intent.putExtra("authorization_key", jsonMainobject.getJSONObject("body").getString("authorization_key"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    intent.putExtra("is_from_social", "");
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }
                            }).show();

                        } else {
                            signUp.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(profilePic);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

}
