package com.spatify.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.spatify.Adapters.AreaAdapter;
import com.spatify.Adapters.CountryAdapter;
import com.spatify.R;
import com.spatify.Util.util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CountryListActivity extends AppCompatActivity {
    @BindView(R.id.title)
    ImageView title;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;


    //ArrayList<AreaModel> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_list);
        ButterKnife.bind(this);


        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myRecyclerView.setAdapter(new CountryAdapter(this, util.list_country));


    }

    @OnClick({R.id.title})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.title:
                finish();
                break;
        }
    }

    public void donetask(int position) {
        Intent intent = new Intent();
        intent.putExtra("name", util.list_country[position]);
        setResult(RESULT_OK, intent);
        finish();
    }
}
