package com.spatify.Activities;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.spatify.Adapters.ImageViewPagerAdapter;
import com.spatify.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class PhotosActivity extends AppCompatActivity {

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.count)
    TextView count;

    String size = "";
    int count_text = 1;
    @BindView(R.id.back_button)
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos);
        ButterKnife.bind(this);
        size = String.valueOf(getIntent().getStringArrayListExtra("image_list").size());
        ImageViewPagerAdapter adapter = new ImageViewPagerAdapter(PhotosActivity.this, getIntent().getStringArrayListExtra("image_list"));
        pager.setAdapter(adapter);
        indicator.setViewPager(pager);

        pager.setCurrentItem(getIntent().getIntExtra("position", 0));
        count.setText(String.valueOf(0 + 1) + "/" + size);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                count.setText(String.valueOf(position + 1) + "/" + size);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        //count.setText("1/" + size);

    }

    @OnClick(R.id.back_button)
    public void onClick() {
        finish();
    }
}
