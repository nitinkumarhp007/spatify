package com.spatify.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateProfileActivity extends AppCompatActivity {
    UpdateProfileActivity context;
    private SavePref savePref;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.camera)
    ImageView camera;
    @BindView(R.id.update)
    Button update;

    Uri fileUri;
    private String selectedimage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);
        setToolbar();

        context = UpdateProfileActivity.this;
        savePref = new SavePref(context);
        setdata();
    }

    private void setdata() {
        Glide.with(context).load(savePref.getImage()).error(R.drawable.user1).into(profilePic);
        name.setText(savePref.getName());
        emailAddress.setText(savePref.getEmail());
        phone.setText(savePref.getPhone());
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Update Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void EDIT_PROFILE_Process() {
        if (ConnectivityReceiver.isConnected()) {
            if (name.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Your Name");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (phone.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Phone");
                update.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                EDIT_PROFILE_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }


    private void EDIT_PROFILE_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (!selectedimage.isEmpty()) {
            final MediaType MEDIA_TYPE = selectedimage.endsWith("png") ?
                    MediaType.parse("image/png") : MediaType.parse("image/jpeg");

            File file = new File(selectedimage);
            formBuilder.addFormDataPart(Parameters.IMAGE, file.getName(), RequestBody.create(MEDIA_TYPE, file));
        }

        formBuilder.addFormDataPart(Parameters.FIRST_NAME, name.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PHONE, phone.getText().toString().trim());

        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.EDIT_PROFILE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setEmail(body.getString("email"));
                            savePref.setName(body.getString("first_name"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setImage(body.getString("image"));
                            finish();
                            util.showToast(context, "Profile Updated Sucessfully!!!");
                        } else {
                            update.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake));
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();

                    selectedimage = getAbsolutePath(this, resultUri);

                    Glide.with(this).load(selectedimage).into(profilePic);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }
            }
        }
    }

    public String getAbsolutePath(Context activity, Uri uri) {

        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    @OnClick({R.id.camera, R.id.profile_pic, R.id.update})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_pic:
                camera();
                break;
            case R.id.camera:
                camera();
                break;
            case R.id.update:
                EDIT_PROFILE_Process();
                break;
        }
    }
    private boolean checkIfAlreadyhavePermission() {
        int result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int result3 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED && result3 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private void requestForSpecificPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CropImage.activity(fileUri)
                            .setAspectRatio(2, 2)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .start(this);
                } else {
                    //not granted
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void camera()
    {
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            }
            else {
                CropImage.activity(fileUri)
                        .setAspectRatio(2, 2)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(this);
            }
        }
        else {
            CropImage.activity(fileUri)
                    .setAspectRatio(2, 2)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }
    }
}
