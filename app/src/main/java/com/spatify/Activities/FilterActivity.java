package com.spatify.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.facebook.internal.Utility;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.innovattic.rangeseekbar.RangeSeekBar;
import com.spatify.Adapters.FilterServiceFacilitiesAdapter;
import com.spatify.DataModel.FacilityServiceModel;
import com.spatify.DataModel.ProviderModel;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.GridAutofitLayoutManager;
import com.spatify.Util.GridSpacing;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsyncGet;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FilterActivity extends AppCompatActivity {
    FilterActivity context;
    @BindView(R.id.search_bar)
    EditText searchBar;
    @BindView(R.id.category_rv)
    RecyclerView categoryRv;
    @BindView(R.id.facilities_rv)
    RecyclerView facilitiesRv;
    @BindView(R.id.service_rv)
    RecyclerView serviceRv;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.gender)
    TextView gender;
    @BindView(R.id.country)
    TextView country;
    @BindView(R.id.min_cost)
    TextView minCost;
    @BindView(R.id.max_cost)
    TextView maxCost;
    @BindView(R.id.seekbar_cost)
    RangeSeekBar seekbarCost;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.apply)
    TextView apply;
    @BindView(R.id.spa)
    TextView spa;
    @BindView(R.id.masseur)
    TextView masseur;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.area)
    TextView area;
    private SavePref savePref;
    ArrayList<ProviderModel> list;
    String latitude = "", longitude = "", gender_text = "", service = "", facility = "";

    private final static int PLACE_PICKER_REQUEST = 34234;
    ArrayList<FacilityServiceModel> facility_list;
    ArrayList<FacilityServiceModel> services_list;

    String category_id = "", min_cost = "", max_cost = "", areas_api = "";

    boolean from_home = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        setToolbar();

        context = FilterActivity.this;
        savePref = new SavePref(context);

        from_home = getIntent().getBooleanExtra("from_home", false);

        if (from_home) {
            /*if (category_id.isEmpty()) {
                category_id = "1";
            }*/
            if (ConnectivityReceiver.isConnected()) {
                ALL_ATTRIBUTES();
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }
        } else {

            //category_id = getIntent().getStringExtra("category_id");

            facility_list = getIntent().getParcelableArrayListExtra("facility_list");
            services_list = getIntent().getParcelableArrayListExtra("services_list");


            service = getIntent().getStringExtra("service");

            facility = getIntent().getStringExtra("facility");

            latitude = getIntent().getStringExtra("latitude");

            longitude = getIntent().getStringExtra("longitude");

            gender_text = getIntent().getStringExtra("gender");
            if (!getIntent().getStringExtra("gender").isEmpty()) {
                if (getIntent().getStringExtra("gender").equals("1")) {
                    gender.setText("Male");
                } else {
                    gender.setText("Female");
                }
            }
            area.setText(getIntent().getStringExtra("area"));
            location.setText(getIntent().getStringExtra("location"));
            country.setText(getIntent().getStringExtra("country"));
            searchBar.setText(getIntent().getStringExtra("search"));

            min_cost = getIntent().getStringExtra("min_cost");
            max_cost = getIntent().getStringExtra("max_cost");

            if (!min_cost.isEmpty()) {
                seekbarCost.setMinThumbValue(Integer.valueOf(min_cost));
                minCost.setText("$" + min_cost);
            }

            if (!max_cost.isEmpty()) {
                seekbarCost.setMaxThumbValue(Integer.valueOf(max_cost));


                maxCost.setText("$" + max_cost);
            }


            if (!getIntent().getStringExtra("rating").isEmpty())
                ratingBar.setRating(Float.parseFloat(getIntent().getStringExtra("rating")));

            if (services_list.size() == 0) {
                if (ConnectivityReceiver.isConnected()) {
                    ALL_ATTRIBUTES();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            } else {
                facilitiesRv.setLayoutManager(new GridLayoutManager(context, 3));
                facilitiesRv.setAdapter(new FilterServiceFacilitiesAdapter(context, facility_list));

                serviceRv.setLayoutManager(new GridLayoutManager(context, 3));
                serviceRv.setAdapter(new FilterServiceFacilitiesAdapter(context, services_list));

                scrollView.setVisibility(View.VISIBLE);

            }


        }

       /* if (category_id.equals("1")) {
            spa.setBackground(getResources().getDrawable(R.drawable.drawable_button_sel));
            masseur.setBackground(getResources().getDrawable(R.drawable.drawable_button));
            spa.setTextColor(getResources().getColor(R.color.white));
            masseur.setTextColor(getResources().getColor(R.color.black));
        } else if (category_id.equals("2")) {
            masseur.setBackground(getResources().getDrawable(R.drawable.drawable_button_sel));
            spa.setBackground(getResources().getDrawable(R.drawable.drawable_button));
            masseur.setTextColor(getResources().getColor(R.color.white));
            spa.setTextColor(getResources().getColor(R.color.black));
        }*/



        /* seekbarCost.

         */

        seekbarCost.setSeekBarChangeListener(new RangeSeekBar.SeekBarChangeListener() {
            @Override
            public void onStartedSeeking() {

            }

            @Override
            public void onStoppedSeeking() {

            }

            @Override
            public void onValueChanged(int minValue, int maxValue) {
                min_cost = String.valueOf(minValue);
                max_cost = String.valueOf(maxValue);
                minCost.setText("$" + min_cost);
                maxCost.setText("$" + max_cost);
            }
        });


    }

    private void ALL_ATTRIBUTES() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.ALL_ATTRIBUTES, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                facility_list = new ArrayList<>();
                services_list = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonmainObject.getJSONObject("body");

                            JSONArray Facility = body.getJSONArray("Facility");
                            for (int i = 0; i < Facility.length(); i++) {
                                JSONObject object = Facility.getJSONObject(i).getJSONObject("Facility");
                                FacilityServiceModel facilityServiceModel = new FacilityServiceModel();
                                facilityServiceModel.setId(object.getString("id"));
                                facilityServiceModel.setName(object.getString("name"));
                                facilityServiceModel.setIs_select(false);
                                facility_list.add(facilityServiceModel);
                            }
                            // facilitiesRv.addItemDecoration(new GridSpacing(12));
                            facilitiesRv.setLayoutManager(new GridLayoutManager(context, 3));
                            facilitiesRv.setAdapter(new FilterServiceFacilitiesAdapter(context, facility_list));

                            JSONArray Services = body.getJSONArray("Service");
                            for (int i = 0; i < Services.length(); i++) {
                                JSONObject object = Services.getJSONObject(i).getJSONObject("Service");
                                FacilityServiceModel facilityServiceModel = new FacilityServiceModel();
                                facilityServiceModel.setId(object.getString("id"));
                                facilityServiceModel.setName(object.getString("name"));
                                facilityServiceModel.setIs_select(false);
                                services_list.add(facilityServiceModel);
                            }

                            //serviceRv.addItemDecoration(new GridSpacing(12));
                            serviceRv.setLayoutManager(new GridLayoutManager(context, 3));
                            serviceRv.setAdapter(new FilterServiceFacilitiesAdapter(context, services_list));

                            scrollView.setVisibility(View.VISIBLE);


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Filter");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.area, R.id.spa, R.id.masseur, R.id.location, R.id.gender, R.id.country, R.id.apply})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.area:
                startActivityForResult(new Intent(context, AreaListActivity.class), 666);
                break;
            case R.id.spa:
                category_id = "1";
                spa.setBackground(getResources().getDrawable(R.drawable.drawable_button_sel));
                masseur.setBackground(getResources().getDrawable(R.drawable.drawable_button));
                spa.setTextColor(getResources().getColor(R.color.white));
                masseur.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.masseur:
                category_id = "2";
                spa.setBackground(getResources().getDrawable(R.drawable.drawable_button));
                masseur.setBackground(getResources().getDrawable(R.drawable.drawable_button_sel));
                spa.setTextColor(getResources().getColor(R.color.black));
                masseur.setTextColor(getResources().getColor(R.color.white));
                break;
            case R.id.location:
                startActivityForResult(new Intent(context, AreaListActivity.class), 666);
                //place_picker();
                break;
            case R.id.gender:
                Show_Dialog();
                break;
            case R.id.country:
                /*CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                        picker.dismiss();
                        country.setText(name);
                    }
                });
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");*/
                startActivityForResult(new Intent(context, CountryListActivity.class), 555);
                break;
            case R.id.apply:
                service = "";
                for (int i = 0; i < services_list.size(); i++) {
                    if (services_list.get(i).isIs_select()) {
                        if (service.isEmpty())
                            service = services_list.get(i).getId();
                        else
                            service = service + "," + services_list.get(i).getId();
                    }
                }
                facility = "";
                for (int i = 0; i < facility_list.size(); i++) {
                    if (facility_list.get(i).isIs_select()) {
                        if (facility.isEmpty())
                            facility = facility_list.get(i).getId();
                        else
                            facility = facility + "," + facility_list.get(i).getId();
                    }
                }

                if (from_home) {
                    Intent intent = new Intent(this, ServiceProviderListingActivity.class);
                    intent.putExtra("area", areas_api);
                    intent.putExtra("facility_list", facility_list);
                    intent.putExtra("services_list", services_list);
                    intent.putExtra("category_id", category_id);
                    intent.putExtra("service", service);
                    intent.putExtra("facility", facility);
                    intent.putExtra("location", location.getText().toString());
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("min_cost", min_cost);
                    intent.putExtra("max_cost", max_cost);
                    if (category_id.equals("1"))
                        intent.putExtra("category_name", "Spa");
                    else if (category_id.equals("2"))
                        intent.putExtra("category_name", "Masseur");
                    else
                        intent.putExtra("category_name", "All");
                    intent.putExtra("gender", gender_text);
                    intent.putExtra("country", country.getText().toString().trim());
                    intent.putExtra("search", searchBar.getText().toString().trim());
                    if (String.valueOf(ratingBar.getRating()).equals("0.0"))
                        intent.putExtra("rating", "");
                    else
                        intent.putExtra("rating", String.valueOf(ratingBar.getRating()));
                    intent.putExtra("from_home_filter", true);
                    startActivity(intent);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                } else {
                    if (ConnectivityReceiver.isConnected()) {




                        Intent intent = new Intent();
                        intent.putExtra("area", areas_api);
                        intent.putExtra("category_id", category_id);
                        intent.putExtra("facility_list", facility_list);
                        intent.putExtra("services_list", services_list);
                        intent.putExtra("service", service);
                        intent.putExtra("facility", facility);
                        intent.putExtra("location", location.getText().toString());
                        intent.putExtra("latitude", latitude);
                        intent.putExtra("longitude", longitude);
                        intent.putExtra("min_cost", min_cost);
                        intent.putExtra("max_cost", max_cost);
                        if (category_id.equals("1"))
                            intent.putExtra("category_name", "Spa");
                        else if (category_id.equals("2"))
                            intent.putExtra("category_name", "Masseur");
                        else
                            intent.putExtra("category_name", "All");
                        intent.putExtra("gender", gender_text);
                        intent.putExtra("country", country.getText().toString().trim());
                        intent.putExtra("search", searchBar.getText().toString().trim());
                        if (String.valueOf(ratingBar.getRating()).equals("0.0"))
                            intent.putExtra("rating", "");
                        else
                            intent.putExtra("rating", String.valueOf(ratingBar.getRating()));
                        setResult(RESULT_OK, intent);
                        finish();

                    } else
                        util.IOSDialog(context, util.internet_Connection_Error);
                }

                break;
        }
    }

    private void show_area() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose an Area");
// add a list
        String[] list = {"Ang Mo Kio", "Bedok", "Bishan", "Bukit Batok", "Bukit Merah", "Bukit Panjang", "  Bukit Timah", "Central Area - Raffles Place, Marina, Cecil",
                "Central Area - Tanjong Pagar, Chinatown", "Central Area - Mount Faber, Telok Blangah, Harbourfront",
                "Central Area - Clarke Quay, City Hall", "Central Area - Bugis, Beach Road, Golden Mile",
                "Central Area - Orchard Road, River Valley", "Central Area - Bukit Timah, Holland, Balmoral",
                " Choa Chu Kang", "Clementi", "Geylang", "Hougang", "Jurong East", "Jurong West",
                "Kallang/ Whampoa", "Marine Parade", " Pasir Ris", "Punggol", "Queenstown", "Sembawang",
                "Sengkang", "Serangoon", "Tampines", "Toa Payoh", "Woodlands", "Yishun"};

        builder.setItems(list, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                area.setText(list[position]);
            }
        });
// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void Show_Dialog() {
        final Item[] items = {
                new Item("Male", R.drawable.heart_r),
                new Item("Female", android.R.drawable.ic_menu_gallery),
        };
        ListAdapter adapter = new ArrayAdapter(
                context,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setTextSize(14f);
                //Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                //Add margin between image and text (support various screen densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };


        new AlertDialog.Builder(context)
                .setTitle("Select Gender")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            gender_text = "1";
                            gender.setText("Male");
                        } else if (item == 1) {
                            gender_text = "2";
                            gender.setText("Female");
                        }
                    }
                }).show();
    }

    public static class Item {
        public final String text;
        //  public final int icon;

        public Item(String text, Integer icon) {
            this.text = text;
            //this.icon = icon;
        }

        @Override
        public String toString() {
            return text;
        }

    }

    private void place_picker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            // for activty
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
            // for fragment
            //startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = PlacePicker.getPlace(this, data);
                latitude = String.valueOf(place.getLatLng().latitude);
                longitude = String.valueOf(place.getLatLng().longitude);
                location.setText(place.getName());
            } else if (requestCode == 666) {
                areas_api = data.getStringExtra("areas_api");
                location.setText(data.getStringExtra("area"));
            } else if (requestCode == 555) {
                country.setText(data.getStringExtra("name"));
            }
        }
    }

}


