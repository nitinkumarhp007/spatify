package com.spatify.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.spatify.DataModel.ProviderModel;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.GPSTracker;
import com.spatify.Util.util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class SortActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.latest_post)
    TextView latestPost;
    @BindView(R.id.oldest_post)
    TextView oldestPost;
    @BindView(R.id.most_views)
    TextView mostViews;
    @BindView(R.id.reviews_rating)
    TextView reviewsRating;
    @BindView(R.id.nearest)
    TextView nearest;

    GoogleApiClient googleApiClient;

    Location location = null;
    FusedLocationProviderClient fusedLocationClient = null;
    ArrayList<ProviderModel> list;

    LocationManager locationManager;

    GPSTracker gpsTracker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sort);
        ButterKnife.bind(this);

        gpsTracker = new GPSTracker(this);

        nearest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int MyVersion = Build.VERSION.SDK_INT;
                if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    if (!checkIfAlreadyhavePermission()) {
                        requestForSpecificPermission();
                    } else {
                        if (!gpsTracker.canGetLocation()) {
                            EnableGPSAutoMatically();
                        } else {
                            Intent intent = new Intent();
                            intent.putExtra("response_type", "5");
                            intent.putExtra("name", getResources().getString(R.string.nearest));
                            setResult(RESULT_OK, intent);
                            finish();
                        }

                    }
                } else {
                    if (!gpsTracker.canGetLocation()) {
                        EnableGPSAutoMatically();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("response_type", "5");
                        intent.putExtra("name", getResources().getString(R.string.nearest));
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }




            }
        });
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_NETWORK_STATE);
        int result2 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestForSpecificPermission() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_NETWORK_STATE,Manifest.permission.ACCESS_FINE_LOCATION},
                101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                Log.e("dataaaa", "top");
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!gpsTracker.canGetLocation()) {
                        EnableGPSAutoMatically();
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("response_type", "5");
                        intent.putExtra("name", getResources().getString(R.string.nearest));
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } else {
                   // util.IOSDialog(this, "You need to allow permission");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        Log.e("dataaaa", "last");
    }


    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(SortActivity.this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(SortActivity.this);
                            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    Activity#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for Activity#requestPermissions for more details.
                                return;
                            }
                            util.showToast(SortActivity.this, "Location Service Turn on Successfully");

                            gpsTracker = new GPSTracker(SortActivity.this);


                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(SortActivity.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            // util.showToast(SortActivity.this, "Location service turn on RESOLUTION_REQUIRED");

                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            //util.showToast(SortActivity.this, "Location service turn on SETTINGS_CHANGE_UNAVAILABLE");
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                location = locationList.get(locationList.size() - 1);
                // util.showToast(getActivity(), "lat long " + location.getLatitude());


                /*gpsTracker = new GPSTracker(context);
                //util.showToast(context, "latt " + String.valueOf(gpsTracker.getLatitude()));
                if (ConnectivityReceiver.isConnected()) {
                    AGENCIE("", true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }*/

                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());




               /* new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        intent.putExtra("response_type", "5");
                        intent.putExtra("name", getResources().getString(R.string.nearest));
                        setResult(RESULT_OK, intent);
                        finish();

                        Intent intent1 = new Intent();
                        intent1.putExtra("response_type", "5");
                        intent1.putExtra("name", getResources().getString(R.string.nearest));
                        setResult(RESULT_OK, intent1);
                        finish();
                    }
                }, 1000);*/

                // nearest.performLongClick();

                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };

    @OnClick({R.id.back_button, R.id.latest_post, R.id.oldest_post, R.id.most_views, R.id.reviews_rating})
    public void onClick(View view) {
        Intent intent = new Intent();
        switch (view.getId()) {
            case R.id.back_button:
                break;
            case R.id.latest_post:
                intent.putExtra("response_type", "1");
                intent.putExtra("name", getResources().getString(R.string.latest_post));
                setResult(RESULT_OK, intent);
                break;
            case R.id.oldest_post:
                intent.putExtra("response_type", "2");
                intent.putExtra("name", getResources().getString(R.string.oldest_post));
                setResult(RESULT_OK, intent);
                break;
            case R.id.most_views:
                intent.putExtra("response_type", "3");
                intent.putExtra("name", getResources().getString(R.string.most_views));
                setResult(RESULT_OK, intent);
                break;
            case R.id.reviews_rating:
                intent.putExtra("response_type", "4");
                intent.putExtra("name", getResources().getString(R.string.reviews_rating));
                setResult(RESULT_OK, intent);
                break;
        }
        finish();

    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
