package com.spatify.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.spatify.DataModel.PackageModel;
import com.spatify.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceDetailActivity extends AppCompatActivity {

    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.featured)
    TextView featured;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.description)
    TextView description;

    PackageModel packageModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);
        ButterKnife.bind(this);

        packageModel = getIntent().getExtras().getParcelable("data");

        String s = packageModel.getName();
        if (s.contains("@")) {
            String f = s.substring(0, s.lastIndexOf("@"));
            String l = s.substring(s.lastIndexOf("@"));

            name.setText(f + "\n" + l.substring(1));
        } else {
            name.setText(s);
        }

        price.setText(packageModel.getPrice());
        description.setText(packageModel.getDescription());


        if (packageModel.getIs_featured().equals("1")) {
            featured.setVisibility(View.VISIBLE);
        } else {
            featured.setVisibility(View.INVISIBLE);
        }

    }

    @OnClick(R.id.back_button)
    public void onClick() {
        finish();
    }
}
