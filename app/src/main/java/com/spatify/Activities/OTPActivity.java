package com.spatify.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OTPActivity extends AppCompatActivity {
    OTPActivity context;
    @BindView(R.id.resend)
    Button resend;
    private SavePref savePref;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.num1)
    EditText num1;
    @BindView(R.id.num2)
    EditText num2;
    @BindView(R.id.num3)
    EditText num3;
    @BindView(R.id.num4)
    EditText num4;
    @BindView(R.id.submit)
    Button submit;
    String authorization_key = "";
    String is_from_social = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);

        context = OTPActivity.this;
        savePref = new SavePref(context);

        authorization_key = getIntent().getStringExtra("authorization_key");
        is_from_social = getIntent().getStringExtra("is_from_social");

        num1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num2.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num3.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    num4.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        num4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty()) {
                    util.hideKeyboard(context);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick({R.id.back_button, R.id.resend, R.id.submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.resend:
                RESEND_OTP_API();
                break;
            case R.id.submit:
                OTPTASK();
                break;
        }
    }

    private void OTPTASK() {
        util.hideKeyboard(context);
        if (ConnectivityReceiver.isConnected()) {
            if (num1.getText().toString().isEmpty() || num2.getText().toString().isEmpty() ||
                    num3.getText().toString().isEmpty() || num4.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please enter One Time Password(OTP)");
                submit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                String entered_otp = num1.getText().toString() + num2.getText().toString() + num3.getText().toString() + num4.getText().toString();
                VERIFY_OTP_API(entered_otp);
            }
        } else
            util.showToast(context, util.internet_Connection_Error);
    }

    private void VERIFY_OTP_API(String otp) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.OTP, otp);
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.VERIFY_OTP, formBody, authorization_key) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setAuthorization_key(body.getString("authorization_key"));
                            savePref.setID(body.getString("id"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setName(body.getString("first_name"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setImage(body.getString("image"));
                            savePref.setStringLatest("is_from_social", is_from_social);
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "Welcome to " + getResources().getString(R.string.app_name));
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void RESEND_OTP_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.RESEND_OTP, formBody, authorization_key) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setTitle(context.getResources().getString(R.string.app_name))
                                    .setMessage("New Verification code have been sent to your registered email. Please check your all email folder or spam folder.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
