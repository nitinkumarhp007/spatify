package com.spatify.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignInActivity extends AppCompatActivity {
    SignInActivity context;
    @BindView(R.id.Facebookbutton)
    Button Facebookbutton;
    @BindView(R.id.google)
    ImageView google;
    private SavePref savePref;

    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.forgot_password)
    Button forgotPassword;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.sign_up)
    Button signUp;

    CallbackManager callbackManager;
    String fb_occupation = "", fb_username = "", fb_phone = "", fb_email = "", fb_image = "", social_id = "",
            fb_gender = "", fb_dob = "", fb_firstname = "", fb_lastname = "", socail_id = "";
    ProgressDialog mDialog;

    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions googleSignInOptions;

    private LoginManager loginManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        context = SignInActivity.this;
        savePref = new SavePref(context);

        mDialog = util.initializeProgress(context);


        callbackManager = CallbackManager.Factory.create();
        //googleplus login :)
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();


        Facebookbutton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

        if (ConnectivityReceiver.isConnected()) {
            facebookLogin();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

        Facebookbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    loginManager.logInWithReadPermissions(
                            context,
                            Arrays.asList(
                                    "email",
                                    "public_profile",
                                    "user_birthday"));
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        });

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    signInWithGplus();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        });

    }

    @OnClick({R.id.forgot_password, R.id.sign_in, R.id.sign_up})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.forgot_password:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.sign_in:
                SigninProcess();
                break;
            case R.id.sign_up:
                startActivity(new Intent(this, SignupActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SigninProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                LOGIN_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void LOGIN_API() {
        util.hideKeyboard(context);
        Log.e("device_token__", SavePref.getDeviceToken(SignInActivity.this, "token"));
        String s = SavePref.getDeviceToken(SignInActivity.this, "token");
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USERLOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");
                            savePref.setAuthorization_key(body.getString("authorization_key"));
                            savePref.setID(body.getString("id"));
                            savePref.setEmail(body.getString("email"));
                            savePref.setName(body.getString("first_name"));
                            savePref.setPhone(body.getString("phone"));
                            savePref.setImage(body.getString("image"));
                            savePref.setStringLatest("is_from_social", "0");
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "Login Sucessfully!!!");
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void facebookLogin() {

        loginManager
                = LoginManager.getInstance();
        callbackManager
                = CallbackManager.Factory.create();


       /* Facebookbutton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday",
                "user_friends", "user_photos"));*/
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mDialog.setMessage("please wait.....");
                mDialog.show();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                try {
                                    fb_email = object.getString("email");
                                    savePref.setEmail(fb_email);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    social_id = object.getString("id");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_image = "http://graph.facebook.com/" + social_id + "/picture?type=large";
                                    savePref.setImage(fb_image);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                   /* fb_cover_image = "https://graph.facebook.com/"
                                            + socail_id + "?fields=cover&access_token=" + getCurrentAccessToken().getToken();*/

                                    JSONObject jsonObject = new JSONObject(response.toString());
                                    JSONObject sourceObj = jsonObject.getJSONObject("cover");
                                    String source = sourceObj.getString("source");
                                    // savePref.setCover_pic(source);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_username = object.getString("name");
                                    savePref.setName(fb_username);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_firstname = object.getString("first_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_lastname = object.getString("last_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_gender = object.getString("gender");
                                    fb_gender = fb_gender.substring(0, 1).toUpperCase()
                                            + fb_gender.substring(1, fb_gender.length());
                                    savePref.setGender(fb_gender);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_dob = object.getString("birthday");
                                    //savePref.setDOB(fb_dob);

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                                socialLoginApi(fb_firstname + " " + fb_lastname, "1", fb_email);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
                mDialog.dismiss();
            }

            @Override
            public void onCancel() {
                mDialog.dismiss();
            }


            @Override
            public void onError(FacebookException error) {
                mDialog.dismiss();
            }

        });
    }

    private void socialLoginApi(final String NAME, final String SOCIAL_TYPE, final String EMAIL) {
       // final ProgressDialog mDialog = util.initializeProgress(context);
        //mDialog.show();
        Log.e("fb_image", fb_image);
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.SOCIAL_ID, social_id);
        formBuilder.addFormDataPart(Parameters.IMAGE, fb_image);
        formBuilder.addFormDataPart(Parameters.SOCIAL_TYPE, SOCIAL_TYPE);
        formBuilder.addFormDataPart(Parameters.FIRST_NAME, NAME);
        formBuilder.addFormDataPart(Parameters.EMAIL, EMAIL);
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SOCIAL_LOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = jsonMainobject.getJSONObject("body");


                            if (body.getString("phone").equals("")) {
                                Intent intent = new Intent(context, SignupActivity.class);
                                intent.putExtra("name", NAME);
                                intent.putExtra("email", EMAIL);
                                intent.putExtra("image", fb_image);
                                intent.putExtra("social_id", social_id);
                                intent.putExtra("social_type", SOCIAL_TYPE);
                                intent.putExtra("is_from_social", true);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else if (body.getString("otp_verified").equals("0")) {

                                new IOSDialog.Builder(context)
                                        .setCancelable(false)
                                        .setTitle(context.getResources().getString(R.string.app_name))
                                        .setMessage("Your account is not verified yet, a verification code have been sent to your registered email.Please check your all email folder or spam folder.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(context, OTPActivity.class);
                                        try {
                                            intent.putExtra("authorization_key", body.getString("authorization_key"));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        intent.putExtra("is_from_social", "1");
                                        startActivity(intent);
                                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                    }
                                }).show();

                            } else {
                                savePref.setAuthorization_key(body.getString("authorization_key"));
                                savePref.setID(body.getString("id"));
                                savePref.setEmail(body.getString("email"));
                                savePref.setName(body.getString("first_name"));
                                savePref.setPhone(body.getString("phone"));
                                savePref.setImage(body.getString("image"));
                                savePref.setStringLatest("is_from_social", "1");
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                util.showToast(context, "Login Sucessfully!!!");
                            }


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    //Google Plus Login
    //This function will option signing intent
    private void signInWithGplus() {
        try {
            mDialog.setMessage("please wait.....");
            mDialog.show();
        } catch (Exception e) {

        }

        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //For both google & fb login
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //for fb login
        callbackManager.onActivityResult(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);

        //for google+ login
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        disconnectFromFacebook();
    }

    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }


    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {

        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            social_id = acct.getId();
            fb_username = acct.getGivenName();
            fb_email = acct.getEmail();
            if (acct.getPhotoUrl() != null)
                fb_image = acct.getPhotoUrl().toString();
            // fb_image = "http://picasaweb.google.com/data/entry/api/user/" + social_id + "?alt=json";
            //fb_image = util.getPath(context, personPhoto);
            String username = acct.getDisplayName();
            socialLoginApi(fb_username, "2", fb_email);
            mDialog.setMessage("please wait.....");
            mDialog.show();
        } else {
            //If login fails
            mDialog.dismiss();
            //Toast.makeText(this, "" + result.getStatus().toString(), Toast.LENGTH_LONG).show();
        }

    }
}

