package com.spatify.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class SettingActivity extends AppCompatActivity {
    SettingActivity context;
    @BindView(R.id.change_password)
    TextView changePassword;
    @BindView(R.id.change_password___)
    RelativeLayout changePassword___;
    @BindView(R.id.about_us)
    TextView aboutUs;
    @BindView(R.id.about_us_layout)
    RelativeLayout aboutUsLayout;
    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.terms_layout)
    RelativeLayout termsLayout;
    @BindView(R.id.logout)
    TextView logout;
    @BindView(R.id.logout_layout)
    RelativeLayout logoutLayout;

    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        context = SettingActivity.this;
        savePref = new SavePref(context);
    }

    @OnClick({R.id.change_password, R.id.change_password___, R.id.about_us, R.id.about_us_layout, R.id.terms, R.id.terms_layout, R.id.logout, R.id.logout_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_password:
                startActivity(new Intent(this, ChangePasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password___:
                startActivity(new Intent(this, ChangePasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.about_us:
                break;
            case R.id.about_us_layout:
                break;
            case R.id.terms:
                break;
            case R.id.terms_layout:
                break;
            case R.id.logout:
                LogoutAlert();
                break;
            case R.id.logout_layout:
                LogoutAlert();
                break;
        }
    }

    private void LogoutAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       /* util.showToast(context, "User Logout Successfully!");
                        savePref.clearPreferences();
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);*/

                        LOGOUT_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void LOGOUT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.AUTH_KEY, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            savePref.clearPreferences();
                            util.showToast(context, "User Logout Successfully!");
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            util.showToast(context, jsonObject.getString("message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
