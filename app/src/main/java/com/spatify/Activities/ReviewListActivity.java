package com.spatify.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.spatify.Adapters.RatingAdapter;
import com.spatify.DataModel.ReviewModel;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ReviewListActivity extends AppCompatActivity {
    ReviewListActivity context;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.write)
    TextView write;
    @BindView(R.id.back_button)
    ImageView backButton;
    @BindView(R.id.s5)
    SeekBar s5;
    @BindView(R.id.s4)
    SeekBar s4;
    @BindView(R.id.s3)
    SeekBar s3;
    @BindView(R.id.s2)
    SeekBar s2;
    @BindView(R.id.s1)
    SeekBar s1;
    @BindView(R.id.topPanel)
    RelativeLayout topPanel;
    @BindView(R.id.average_rating)
    TextView averageRating;
    @BindView(R.id.total_reviews)
    TextView totalReviews;
    private SavePref savePref;
    String provider_id = "";

    ArrayList<ReviewModel> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_list);
        ButterKnife.bind(this);

        context = ReviewListActivity.this;
        savePref = new SavePref(context);
        provider_id = getIntent().getStringExtra("provider_id");

        disableseek();


    }


    @Override
    protected void onResume() {
        super.onResume();

        if (ConnectivityReceiver.isConnected())
            PROVIDER_RATINGS();
        else
            util.IOSDialog(context, util.internet_Connection_Error);

    }

    private void PROVIDER_RATINGS() {
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, provider_id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.PROVIDER_RATINGS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body1 = jsonmainObject.getJSONObject("body");
                            JSONArray body = body1.getJSONArray("all_rating");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i);

                                ReviewModel reviewModel = new ReviewModel();
                                reviewModel.setImage(object.getJSONObject("User").getString("image"));
                                reviewModel.setName(object.getJSONObject("User").getString("first_name"));
                                reviewModel.setRating(object.getJSONObject("Review").getString("rating"));
                                reviewModel.setText(object.getJSONObject("Review").getString("review"));
                                reviewModel.setCreated(object.getJSONObject("Review").getString("created"));
                                list.add(reviewModel);
                            }
                            JSONObject ratings = body1.getJSONObject("ratings");
                            int p1 = Integer.parseInt(ratings.getString("rating_1"));
                            s1.setProgress(p1);
                            int p2 = Integer.parseInt(ratings.getString("rating_2"));
                            s2.setProgress(p2);
                            int p3 = Integer.parseInt(ratings.getString("rating_3"));
                            s3.setProgress(p3);
                            int p4 = Integer.parseInt(ratings.getString("rating_4"));
                            s4.setProgress(p4);
                            int p5 = Integer.parseInt(ratings.getString("rating_5"));
                            s5.setProgress(p5);

                            s1.setMax(Integer.parseInt(ratings.getString("review_count")));
                            s2.setMax(Integer.parseInt(ratings.getString("review_count")));
                            s3.setMax(Integer.parseInt(ratings.getString("review_count")));
                            s4.setMax(Integer.parseInt(ratings.getString("review_count")));
                            s5.setMax(Integer.parseInt(ratings.getString("review_count")));

                            // averageRating.setText(ratings.getString("avg_rate"));
                            averageRating.setText(getIntent().getStringExtra("ratingText"));
                            totalReviews.setText(ratings.getString("review_count") + " Rating");


                            if (list.size() > 0) {
                                myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                myRecyclerView.setAdapter(new RatingAdapter(context, list));

                                myRecyclerView.setVisibility(View.VISIBLE);
                                topPanel.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                errorText.setVisibility(View.VISIBLE);
                                myRecyclerView.setVisibility(View.GONE);
                                topPanel.setVisibility(View.GONE);
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @OnClick({R.id.back_button, R.id.write})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_button:
                finish();
                break;
            case R.id.write:
                Intent intent = new Intent(this, RatingPostActivity.class);
                intent.putExtra("provider_id", provider_id);
                startActivity(intent);
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void disableseek() {
        s1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        s2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        s3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        s4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        s5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }
}
