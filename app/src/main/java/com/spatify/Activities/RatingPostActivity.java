package com.spatify.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ligl.android.widget.iosdialog.IOSDialog;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class RatingPostActivity extends AppCompatActivity {
    RatingPostActivity context;
    @BindView(R.id.rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.text)
    EditText text;
    @BindView(R.id.review)
    Button review;
    private SavePref savePref;
    String provider_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_post);
        ButterKnife.bind(this);

        context = RatingPostActivity.this;
        savePref = new SavePref(context);
        provider_id = getIntent().getStringExtra("provider_id");
        setToolbar();
    }

    @OnClick(R.id.review)
    public void onClick() {
        if (ConnectivityReceiver.isConnected()) {
            if (String.valueOf(ratingBar.getRating()).isEmpty()) {
                util.IOSDialog(context, "Please Select Rating");
                review.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (text.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Review");
                review.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                if (savePref.getAuthorization_key().isEmpty()) {
                    util.login_first(context);
                } else {
                    RATE_PROVIDER_API();
                }
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    public void RATE_PROVIDER_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, provider_id);
        formBuilder.addFormDataPart(Parameters.RATING, String.valueOf(ratingBar.getRating()));
        formBuilder.addFormDataPart(Parameters.REVIEW, text.getText().toString().trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.RATE_PROVIDER, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            new IOSDialog.Builder(context)
                                    .setCancelable(false)
                                    .setMessage("Review Posted successfully!, Admin will approve it.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).show();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Write Review");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


}
