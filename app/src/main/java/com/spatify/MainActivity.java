package com.spatify;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.spatify.Activities.AddDetailActivity;
import com.spatify.Activities.SignInActivity;
import com.spatify.Fragments.BlankFragment;
import com.spatify.Fragments.ExploreFragment;
import com.spatify.Fragments.FeaturedListingFragment;
import com.spatify.Fragments.HomeFragment;
import com.spatify.Fragments.NotificationsFragment;
import com.spatify.Fragments.ProfileFragment;
import com.spatify.Fragments.WishlistFragment;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.title)
    TextView title;
    public Toolbar toolbar;
    public static MainActivity context;
    public static BottomNavigationView navigation;
    private SavePref savePref;

    public static boolean is_from_push = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        context = MainActivity.this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        // toolbar.setVisibility(View.GONE);

        savePref = new SavePref(context);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.setSelectedItemId(R.id.navigation_home);

        Log.e("device_token_", SavePref.getDeviceToken(this, "token"));

        is_from_push = getIntent().getBooleanExtra("is_from_push", false);

        if (is_from_push) {
            savePref.setStringLatest("image_push", getIntent().getStringExtra("image"));
            savePref.setStringLatest("message", getIntent().getStringExtra("message"));
        }

        //transparentStatusAndNavigation();
    }

    private void transparentStatusAndNavigation() {
        //make full transparent statusBar
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            /*getWindow().setNavigationBarColor(Color.TRANSPARENT);*/
        }
    }

    private void setWindowFlag(final int bits, boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // toolbar.setVisibility(View.VISIBLE);
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }
            if (Build.VERSION.SDK_INT >= 19) {
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_VISIBLE
                );
            }

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    /*if (Build.VERSION.SDK_INT >= 19) {
                        getWindow().getDecorView().setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        );
                    }
                    window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    if (Build.VERSION.SDK_INT >= 21) {
                        getWindow().setStatusBarColor(Color.TRANSPARENT);
                    }*/

                    transparentStatusAndNavigation();

                    title.setText("Home");
                    loadFragment(new HomeFragment());
                    return true;
                case R.id.navigation_wishlist:
                    if (savePref.getAuthorization_key().isEmpty()) {
                        // toolbar.setVisibility(View.GONE);
                        login_first(MainActivity.this);
                    } else {
                        loadFragment(new WishlistFragment());
                    }
                    title.setText("Wishlist");
                    return true;
                case R.id.navigation_explore:
                    transparentStatusAndNavigation();
                    title.setText("Explore");
                    loadFragment(new ExploreFragment());
                    return true;
                case R.id.navigation_notification:
                    title.setText("Featured");
                    loadFragment(new FeaturedListingFragment());
                    return true;
                case R.id.navigation_profile:
                    if (savePref.getAuthorization_key().isEmpty()) {
                        //toolbar.setVisibility(View.GONE);
                        login_first(MainActivity.this);
                    } else {
                        title.setText("Profile");
                        loadFragment(new ProfileFragment());
                    }
                    return true;
            }
            return false;
        }
    };




    public void login_first(Context context) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("You need to Login First").setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, SignInActivity.class);
                intent.putExtra("from_home", true);
                context.startActivity(intent);
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        navigation.setSelectedItemId(R.id.navigation_home);
                    }
                }).show();
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        shareopencheck();
        Log.e("taskkkkk", "onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("taskkkkk", "onRestart");
        shareopencheck();
    }

    private void shareopencheck() {
        try {
            //check_notification_flag = true;
            final Intent intent = getIntent();
            Uri data = intent.getData();
            Log.e("data_share", data.toString());
            String post_id = data.toString().substring(data.toString().lastIndexOf("/") + 1);
            Log.e("post_id", post_id);

            Intent intent1 = new Intent(context, AddDetailActivity.class);
            intent1.putExtra("is_deep_link", true);
            intent1.putExtra("id", post_id);//133
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent1);

        } catch (Exception e) {
            String s = e.toString();
            Log.e("taskkkkk", s);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
