package com.spatify.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.spatify.Activities.SignInActivity;
import com.spatify.Activities.SplashActivity;
import com.spatify.Adapters.ExplorelistAdapter;
import com.spatify.Adapters.MarkerInfoWindowAdapter;
import com.spatify.DataModel.MapModel;
import com.spatify.DataModel.ProviderModel;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.GPSTracker;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static androidx.core.content.ContextCompat.checkSelfPermission;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveCanceledListener,
        GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraMoveListener {


    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.discrete_sv)
    DiscreteScrollView discrete_sv;
    @BindView(R.id.spa)
    RadioButton spa;
    @BindView(R.id.masseurs)
    RadioButton masseurs;
    @BindView(R.id.search_bar)
    TextView searchBar;
    @BindView(R.id.icon8_current)
    ImageView icon8Current;
    @BindView(R.id.both)
    RadioButton both;
    @BindView(R.id.text)
    TextView text;

    private GoogleMap mMap;
    private float currentZoom = 15;
    GPSTracker gpsTracker = null;
    private SavePref savePref;
    Unbinder unbinder;
    Context context;
    private final static int PLACE_PICKER_REQUEST = 34234;
    String latitude = "", longitude = "";
    String latitude_current = "", longitude_current = "";

    Marker marker;

    boolean isGPS = false;

    GoogleApiClient googleApiClient;

    Location location = null;
    FusedLocationProviderClient fusedLocationClient = null;
    ArrayList<ProviderModel> list;

    LocationManager locationManager;

    boolean permission = false;


    public ExploreFragment() {
        // Required empty public constructor
    }


    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        //initi();

        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            } else {
                permission = true;
                initi();

            }
        } else {
            permission = true;
            initi();
        }


        text.setVisibility(View.INVISIBLE);


        spa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permission) {
                    spa.setChecked(true);
                    masseurs.setChecked(false);
                    if (ConnectivityReceiver.isConnected())
                        PROVIDER_LISTING("1");
                    else
                        util.IOSDialog(context, util.internet_Connection_Error);
                } else {
                    requestForSpecificPermission();
                }

            }
        });
        masseurs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permission) {
                    masseurs.setChecked(true);
                    spa.setChecked(false);
                    if (ConnectivityReceiver.isConnected())
                        PROVIDER_LISTING("2");
                    else
                        util.IOSDialog(context, util.internet_Connection_Error);
                } else {
                    requestForSpecificPermission();
                }

            }
        });

        discrete_sv.addScrollStateChangeListener(new DiscreteScrollView.ScrollStateChangeListener<RecyclerView.ViewHolder>() {
            @Override
            public void onScrollStart(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

            }

            @Override
            public void onScrollEnd(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                Move_map(i, false);
                Log.e("Top", "scroll");
                // text.setText(String.valueOf(i + 1) + "/" + String.valueOf(list.size()));


            }

            @Override
            public void onScroll(float v, int i, int i1, @Nullable RecyclerView.ViewHolder viewHolder, @Nullable RecyclerView.ViewHolder t1) {

            }
        });


        icon8Current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permission) {
                    LatLng latLng = new LatLng(Double.parseDouble(latitude_current),
                            Double.parseDouble(longitude_current));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            latLng, currentZoom);
                    Log.e("ZOOM__", String.valueOf(currentZoom));
                    mMap.animateCamera(location, 2100, null);
                } else {
                    requestForSpecificPermission();
                }


            }
        });

        return view;
    }

    private void initi() {

        gpsTracker = new GPSTracker(context);

        latitude = String.valueOf(gpsTracker.getLatitude());
        latitude_current = String.valueOf(gpsTracker.getLatitude());
        longitude = String.valueOf(gpsTracker.getLongitude());
        longitude_current = String.valueOf(gpsTracker.getLongitude());

        Log.e("current_location-", String.valueOf(gpsTracker.getLatitude()) + " " + String.valueOf(gpsTracker.getLongitude()));
        SupportMapFragment mapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.proselectlocationmapid);
        mapFragment.getMapAsync(this);

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                2000,
                10, locationListenerGPS);

        if (!gpsTracker.canGetLocation())
            EnableGPSAutoMatically();

    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_NETWORK_STATE);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestForSpecificPermission() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION},
                101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                Log.e("dataaaa", "top");
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initi();
                } else {
                    //  util.IOSDialog(context, "You need to allow permission");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        Log.e("dataaaa", "last");
    }


    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            String msg = "New Latitude: " + latitude + "New Longitude: " + longitude;
            //   Toast.makeText(context, msg, Toast.LENGTH_LONG).show();

            // Log.e("callllll", msg);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


   /* @Override
    public void onResume() {
        super.onResume();

       *//* if (!gpsTracker.canGetLocation())
            EnableGPSAutoMatically();*//*

        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForSpecificPermission();
            } else {
                if (!gpsTracker.canGetLocation())
                    EnableGPSAutoMatically();

            }
        } else {
            if (!gpsTracker.canGetLocation())
                EnableGPSAutoMatically();
        }
        Log.e("dataaaa","onResume");


    }*/

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(1 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // util.showToast(context, "GPS Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.

                            if (fusedLocationClient == null)
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext());
                            if (checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    Activity#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for Activity#requestPermissions for more details.
                                return;
                            }
                            fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());


                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // util.showToast(context, "GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }


                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // util.showToast(context, "Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                location = locationList.get(locationList.size() - 1);
                // util.showToast(getActivity(), "lat long " + location.getLatitude());


                /*gpsTracker = new GPSTracker(context);
                //util.showToast(context, "latt " + String.valueOf(gpsTracker.getLatitude()));
                if (ConnectivityReceiver.isConnected()) {
                    AGENCIE("", true);
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }*/

                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                CameraUpdate location11 = CameraUpdateFactory.newLatLngZoom(
                        latLng, 13);
                mMap.animateCamera(location11, 2100, null);

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        CameraUpdate location1 = CameraUpdateFactory.newLatLngZoom(
                                latLng, currentZoom);
                        Log.e("ZOOM__", String.valueOf(currentZoom));
                        mMap.animateCamera(location1, 2100, null);

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {

                                latitude = String.valueOf(location.getLatitude());
                                latitude_current = String.valueOf(location.getLatitude());
                                longitude = String.valueOf(location.getLongitude());
                                longitude_current = String.valueOf(location.getLongitude());

                                if (ConnectivityReceiver.isConnected())
                                    PROVIDER_LISTING("2");
                                else
                                    util.IOSDialog(context, util.internet_Connection_Error);

                            }
                        }, 2000);

                    }
                }, 3000);

                fusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        }
    };


    private void PROVIDER_LISTING(String category_id) {

        Log.e("lat_long", latitude + " " + longitude);
        if (discrete_sv != null)
            discrete_sv.setVisibility(View.INVISIBLE);
        if (searchBar != null)
            searchBar.setVisibility(View.GONE);
        if (icon8Current != null)
            icon8Current.setVisibility(View.GONE);
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, category_id);// 1 for spa ,2= massuer
        formBuilder.addFormDataPart(Parameters.USER_ID, savePref.getID());
        formBuilder.addFormDataPart(Parameters.IS_FEATURED, "");
        //    formBuilder.addFormDataPart(Parameters.LATITUDE, "1.277240");
        //  formBuilder.addFormDataPart(Parameters.LONGITUDE, "103.830220");

      //  formBuilder.addFormDataPart(Parameters.LATITUDE, "1.298570");
        //formBuilder.addFormDataPart(Parameters.LONGITUDE, "103.855690");

        formBuilder.addFormDataPart(Parameters.LATITUDE, latitude);
        formBuilder.addFormDataPart(Parameters.LONGITUDE, longitude);

        formBuilder.addFormDataPart(Parameters.IS_NEARBY, "1");
        formBuilder.addFormDataPart(Parameters.RADIUS, "2");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FILTER_PROVIDER_LISTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                mMap.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i).getJSONObject("Provider");
                                ProviderModel providerModel = new ProviderModel();
                                if (i == 0)
                                    providerModel.setIs_selected("1");
                                else
                                    providerModel.setIs_selected("0");

                                providerModel.setId(object.getString("id"));
                                providerModel.setFirst_name(object.getString("first_name"));
                                providerModel.setView(object.getString("view"));
                               /* if (object.getString("category_name").equals("Spa")) {
                                    providerModel.setId_show("S0" + object.getString("id_show"));
                                } else {
                                    providerModel.setId_show("M0" + object.getString("id_show"));
                                }*/
                                providerModel.setReview(object.getString("review"));
                                providerModel.setRating(object.getString("rating"));
                                providerModel.setIs_like(object.optString("is_like"));
                                providerModel.setCurrent_count("0");
                                providerModel.setCount("0");
                                providerModel.setLat(object.optString("latitude"));
                                providerModel.setLng(object.optString("longitude"));
                                //  providerModel.setAddress(object.optString("address"));
                                if (body.getJSONObject(i).getJSONArray("Image").length() > 0)
                                    providerModel.setImage(body.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                list.add(providerModel);










                              /*
                              old code

                              LatLng latLng = new LatLng(Double.parseDouble(object.getString("latitude")),
                                        Double.parseDouble(object.getString("longitude")));

                                marker = mMap.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.spatify_marker)));
                                marker.setTag(i);
                                */


                            }

                            //old
                            LatLng latLng1 = new LatLng(Double.parseDouble(latitude_current), Double.parseDouble(longitude_current));

                            /*TextView text = new TextView(context);
                            text.setText("1");
                            IconGenerator generator = new IconGenerator(context);
                            generator.setTextAppearance(R.style.iconGenText);
                            generator.setBackground(getResources().getDrawable(R.drawable.spatify_marker));
                            generator.setContentView(text);
                            Bitmap icon1 = generator.makeIcon();*/


                            marker = mMap.addMarker(new MarkerOptions()
                                    .position(latLng1)
                                    .draggable(true)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_3x)));
                            marker.setTag(999);


                            for (int i = 0; i < list.size(); i++) {

                                if (list.size() > 0) {
                                    if (list.get(0).getLat().equals(list.get(i).getLat())) {
                                        list.get(i).setIs_selected("1");
                                    }
                                }

                                int n = 0;
                                for (int j = 0; j < list.size(); j++) {
                                    if (list.get(i).getLat().equals(list.get(j).getLat())) {

                                        n = n + 1;
                                        list.get(i).setCurrent_count(String.valueOf(Integer.parseInt(list.get(i).getCurrent_count()) + 1));

                                        if (!list.get(j).isIs_changed()) {
                                            list.get(j).setCount(String.valueOf(Integer.parseInt(list.get(j).getCount()) + n));
                                            list.get(j).setIs_changed(true);
                                        }

                                    }
                                }


//            marker.showInfoWindow();
//            marker.setTag(latLng);
                            }

//from here
                           /* for (int i = 0; i < list.size(); i++) {
                                LatLng latLng = new LatLng(Double.parseDouble(list.get(i).getLat()),
                                        Double.parseDouble(list.get(i).getLng()));
                                IconGenerator generator = new IconGenerator(getActivity());

                                int layout = R.layout.marker_view_new;
                                if (list.get(i).getIs_selected().equals("1")) {
                                    if (list.get(i).getCurrent_count().equals("1")) {
                                        //generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.marker_sel));
                                        generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_s));
                                        layout = R.layout.marker_view_2;
                                    } else {
                                        // generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_u_s));

                                        //  new issue
                                        //    generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ms1));
                                        generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.mssssssss));

                                        if (Integer.valueOf(list.get(i).getCurrent_count()) > 9) {
                                            layout = R.layout.marker_view_new_plus;

                                            Log.e("data___", "10_after");

                                        }

                                        //layout = R.layout.marker_view_new_api;


                                        //only change
                                    }

                                } else {
                                    if (list.get(i).getCurrent_count().equals("1")) {
                                        // generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.spatify_marker));
                                        generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_u));
                                        layout = R.layout.marker_view_2;
                                    } else {
                                        //generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_u_s));
                                        generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ms1));
                                        //layout = R.layout.marker_view_new_api;

                                        if (Integer.valueOf(list.get(i).getCurrent_count()) > 9) {
                                            layout = R.layout.marker_view_new_plus;

                                            Log.e("data___", "10_after");

                                        }

                                    }

                                }

                                LayoutInflater myInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View activityView = myInflater.inflate(layout, null, false);


                                TextView textView = (TextView) activityView.findViewById(R.id.text_view);
                                if (list.get(i).getCurrent_count().equals("1"))
                                    textView.setText("");
                                else {
                                    *//*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                                    params.setMargins(48, 9, 0, 0);
                                    textView.setLayoutParams(params);*//*
                                    textView.setText(list.get(i).getCurrent_count());
                                }


                                generator.setContentView(activityView);

                                // generator.setContentView(text);
                                Bitmap icon = generator.makeIcon();
                                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true)
                                        .icon(BitmapDescriptorFactory.fromBitmap(icon)));
                                marker.setTag(latLng);

                            }
                            */ //to here


                            if (list.size() > 0) {
                                text.setText("1/" + String.valueOf(list.size()));

                                discrete_sv.setVisibility(View.VISIBLE);
                                text.setVisibility(View.INVISIBLE);
                                discrete_sv.setAdapter(new ExplorelistAdapter(context, list, ExploreFragment.this));
                                Log.e("Top", "api");
                                Move_map(0, false);
                            } else {
                                discrete_sv.setVisibility(View.INVISIBLE);
                                text.setVisibility(View.INVISIBLE);
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void Move_map(int position, boolean is_click) {

        if (is_click) {
            LatLng latLng = new LatLng(Double.parseDouble(list.get(position).getLat()), Double.parseDouble(list.get(position).getLng()));
            // mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            //mMap.getUiSettings().setMyLocationButtonEnabled(true);
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    latLng, 15);
            mMap.animateCamera(location, 2300, null);
        } else {
            mMap.clear();
            LatLng latLng1 = new LatLng(Double.parseDouble(latitude_current), Double.parseDouble(longitude_current));
            marker = mMap.addMarker(new MarkerOptions().draggable(true)
                    .position(latLng1)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.pin_3x))
                    .draggable(true)
            );
            marker.setTag(999);

            //new code
            // HashMap<LatLng, Integer> hashMap = new HashMap<>(list.size());
            for (int i = 0; i < list.size(); i++) {

            /*if (i == 0)
                hashMap.put(latLng, 1);
            else {
                if (hashMap.containsKey(latLng)) {
                    int valu = hashMap.get(latLng) + 1;
                    hashMap.put(latLng, valu);
                } else {
                    hashMap.put(latLng, 1);
                }
            }*/
           /* BitmapDescriptor icon = null;


            if (list.get(i).getIs_selected().equals("1")) {
                icon = BitmapDescriptorFactory.fromResource(R.drawable.marker_sel);
            } else {
                icon = BitmapDescriptorFactory.fromResource(R.drawable.spatify_marker);
            }
            marker = mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .draggable(true)
                    .icon(icon));
            marker.setTag(latLng);
            marker.showInfoWindow();*/
            }
            // mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter(getActivity(), hashMap));


            for (int i = 0; i < list.size(); i++) {
                LatLng latLng = new LatLng(Double.parseDouble(list.get(i).getLat()),
                        Double.parseDouble(list.get(i).getLng()));

                if (i == position || list.get(position).getLat().equals(list.get(i).getLat()))
                    list.get(i).setIs_selected("1");
                else
                    list.get(i).setIs_selected("0");

                // TextView text = new TextView(getActivity());
                // text.setText(hashMap.get(latLng).toString());
                //text.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));
                IconGenerator generator = new IconGenerator(getActivity());
                int layout = R.layout.marker_view_new;//change
                if (list.get(i).getIs_selected().equals("1")) {
                    if (list.get(i).getCurrent_count().equals("1")) {
                        generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_s));
                        layout = R.layout.marker_view_2;
                    } else {
                        //generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.adi_2));
                        //generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_l));
                        generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ml1));
                        if (Integer.valueOf(list.get(position).getCount()) > 9) {
                            layout = R.layout.marker_view_plus;
                            Log.e("more_", "t_" + list.get(i).getCount());
                        } else if ((Integer.valueOf(list.get(position).getCount()) < 10) && (Integer.valueOf(list.get(position).getCurrent_count()) > 9)) {
                            layout = R.layout.marker_view_xxx;//change
                            Log.e("more_", "samuu" + list.get(i).getCount());
                        } else {
                            layout = R.layout.marker_view;//change
                            Log.e("more_", "s_" + list.get(i).getCount());
                        }

                        //only change
                    }

                } else {
                    if (list.get(i).getCurrent_count().equals("1")) {
                        generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_u));
                        layout = R.layout.marker_view_2;
                    } else {

                        if (Integer.valueOf(list.get(i).getCurrent_count()) > 9) {
                            layout = R.layout.marker_view_new_plus;
                        }
                        //  generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_u_s));
                        // generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.m_ss));
                        generator.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ms1));
                    }

                }

                LayoutInflater myInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View activityView = myInflater.inflate(layout, null, false);

                TextView textView = (TextView) activityView.findViewById(R.id.text_view);

                if (list.get(i).getCurrent_count().equals("1"))
                    textView.setText("");
                else {
                    if (list.get(i).getLat().equals(list.get(position).getLat())) {
                        textView.setText(list.get(position).getCount() + " of " + list.get(i).getCurrent_count());
                    } else {
                   /* RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    params.setMargins(48, 9, 0, 0);
                    textView.setLayoutParams(params);*/

                        textView.setText(list.get(i).getCurrent_count());
                    }
                }


                generator.setContentView(activityView);

                // generator.setContentView(text);
                Bitmap icon = generator.makeIcon();
                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).draggable(true)
                        .icon(BitmapDescriptorFactory.fromBitmap(icon)));
                marker.setTag(latLng);

//            marker.showInfoWindow();
//            marker.setTag(latLng);
            }


            LatLng latLng = new LatLng(Double.parseDouble(list.get(position).getLat()), Double.parseDouble(list.get(position).getLng()));
            // mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            //mMap.getUiSettings().setMyLocationButtonEnabled(true);
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    latLng, 15);
            Log.e("ZOOM__", "here_yes" + String.valueOf(currentZoom));
            mMap.animateCamera(location, 2100, null);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }


    }


    public void DO_LIKE_API(int position) {
        String status = "";
        if (list.get(position).getIs_like().equals("1")) {
            status = "0";
        } else {
            status = "1";
        }
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, list.get(position).getId());
        formBuilder.addFormDataPart(Parameters.STATUS, status);
        RequestBody formBody = formBuilder.build();
        String finalStatus = status;
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.DO_LIKE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            list.get(position).setIs_like(finalStatus);
                            discrete_sv.setAdapter(new ExplorelistAdapter(context, list, ExploreFragment.this));
                        } else {
                            util.IOSDialog(context, jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (gpsTracker.canGetLocation()) {
            LatLng latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
          /*  mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("")
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.pin_3x))
                    .draggable(true)
            );*/

            marker = mMap.addMarker(new MarkerOptions().draggable(true)
                    .position(latLng)
                    .icon(BitmapDescriptorFactory
                            .fromResource(R.drawable.pin_3x))
                    .draggable(true)
            );
            marker.setTag(999);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    latLng, 13);
            mMap.animateCamera(location, 2100, null);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                            latLng, currentZoom);
                    Log.e("ZOOM__", String.valueOf(currentZoom));
                    mMap.animateCamera(location, 2100, null);

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            if (ConnectivityReceiver.isConnected())
                                PROVIDER_LISTING("2");
                            else
                                util.IOSDialog(context, util.internet_Connection_Error);

                        }
                    }, 2000);

                }
            }, 3000);

        }


        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //get latlng at the center by calling
                LatLng midLatLng = mMap.getCameraPosition().target;

                Log.e("callllll", "end" + String.valueOf(midLatLng.latitude) + "  " + String.valueOf(midLatLng.longitude));

                latitude = String.valueOf(midLatLng.latitude);
                longitude = String.valueOf(midLatLng.longitude);


            }
        });

        // mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);

//Don't forget to Set draggable(true) to marker, if this not set marker does not drag.
       /* mMap.addMarker(new MarkerOptions()
                .position(crntLocationLatLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_my_location))
                .draggable(true));*/

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //int position = (int) (marker.getTag());

                LatLng latLng = (LatLng) marker.getTag();

                int position = 0;
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getLat().equals(String.valueOf(latLng.latitude))) {
                        position = i;
                        break;
                    }

                }
                if (position != 999) {
                    Log.e("Top", "yes");
                    discrete_sv.smoothScrollToPosition(position);
                }

                if (discrete_sv.getCurrentItem() == position) {
                    Log.e("Top", "current");
                    Move_map(position, false);
                }

                return false;
            }
        });
    }

    @OnClick(R.id.search_bar)
    public void onClick() {

        if (permission) {
            //  place_picker();
            if (spa.isChecked()) {
                PROVIDER_LISTING("1");
            } else {
                PROVIDER_LISTING("2");
            }
        } else {
            requestForSpecificPermission();
        }


    }

    private void place_picker() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            // for activty
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
            // for fragment
            //startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location__) {
        location = location__;

        Log.e("onlocation_", String.valueOf(location.getLatitude() + "  " + String.valueOf(location.getLongitude())));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraIdle() {
       /* Toast.makeText(context, "The camera is onCameraIdle.",
                Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onCameraMoveCanceled() {
       /* Toast.makeText(context, "The camera is canceled.",
                Toast.LENGTH_SHORT).show();*/


    }

    @Override
    public void onCameraMove() {
        /*Toast.makeText(context, "The camera is moving.",
                Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onCameraMoveStarted(int reason) {

        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
           /* Toast.makeText(context, "The user gestured on the map.",
                    Toast.LENGTH_SHORT).show();*/

            searchBar.setVisibility(View.VISIBLE);
            icon8Current.setVisibility(View.VISIBLE);
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
           /* Toast.makeText(context, "The user tapped something on the map.",
                    Toast.LENGTH_SHORT).show();*/
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
            /*Toast.makeText(context, "The app moved the camera.",
                    Toast.LENGTH_SHORT).show();*/
        }

    }
}
