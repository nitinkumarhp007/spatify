package com.spatify.Fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.spatify.Activities.FilterActivity;
import com.spatify.Activities.ServiceProviderListingActivity;
import com.spatify.Activities.SignInActivity;
import com.spatify.Adapters.AddListingHomeAdapter;
import com.spatify.Adapters.HomeViewPagerAdapter;
import com.spatify.DataModel.ProviderModel;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.angeldevil.autoscrollviewpager.AutoScrollViewPager;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    Context context;
    Unbinder unbinder;

    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.my_recycler_view_latest_spa)
    RecyclerView myRecyclerViewLatestSpa;
    @BindView(R.id.my_recycler_view_latest_masseur)
    RecyclerView myRecyclerViewLatestMasseur;
    @BindView(R.id.pager)
    AutoScrollViewPager pager;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.search_bar)
    TextView search_bar;

    ArrayList<ProviderModel> top_list;
    ArrayList<ProviderModel> list;
    ArrayList<ProviderModel> list_masseur;
    @BindView(R.id.spa)
    LinearLayout spa;
    @BindView(R.id.masseur)
    LinearLayout masseur;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.close_main)
    ImageView close_main;

    private SavePref savePref;
    ProgressDialog mDialog;
    Timer timer;
    HomeViewPagerAdapter adapter;

    Dialog dialog;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();

        savePref = new SavePref(context);
        mDialog = util.initializeProgress(context);


        //  util.IOSDialog(context, SavePref.getDeviceToken(context, "token"));
        // transparentStatusAndNavigation();

        close_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close_main.setVisibility(View.GONE);
                if (close_main.getVisibility() == View.VISIBLE) {
                    if (dialog != null)
                        dialog.dismiss();
                }
            }
        });


        return view;


    }


    private void transparentStatusAndNavigation() {
        //make full transparent statusBar
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT);
            /*getWindow().setNavigationBarColor(Color.TRANSPARENT);*/
        }
    }

    private void setWindowFlag(final int bits, boolean on) {
        Window win = getActivity().getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    private void STORE_DEVICE_TOKEN() {
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(context, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.STORE_DEVICE_TOKEN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            //  util.showToast(context, "Token Updated Sucessfully!!!"+SavePref.getDeviceToken(context, "token"));
                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        // showDialog(getActivity(), savePref.getStringLatest("message"), savePref.getStringLatest("image_push"));
       // showDialog(getActivity(),"","");
        if (MainActivity.is_from_push) {
            MainActivity.is_from_push = false;
            if (!savePref.getStringLatest("image_push").isEmpty())
                showDialog(getActivity(), savePref.getStringLatest("message"), savePref.getStringLatest("image_push"));
            else {
                if (ConnectivityReceiver.isConnected()) {
                    PROVIDER_LISTING("1");
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
            //Toast.makeText(context, image, Toast.LENGTH_SHORT).show();
        } else {
            if (ConnectivityReceiver.isConnected()) {
                PROVIDER_LISTING("1");
            } else {
                util.IOSDialog(context, util.internet_Connection_Error);
            }
        }


    }

    public void showDialog(Activity activity, String msg, String image) {
        Log.e("image", image);
        dialog = new Dialog(activity,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);

        RelativeLayout middle = (RelativeLayout) dialog.findViewById(R.id.middle);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        ImageView image_ = (ImageView) dialog.findViewById(R.id.image);
        ProgressBar progress_bar = (ProgressBar) dialog.findViewById(R.id.progress_bar);


        image_.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
               // dialog.dismiss();
                return false;
            }
        });

        Glide.with(context)
                .load(image)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progress_bar.setVisibility(View.GONE);
                        middle.setVisibility(View.GONE);
                        close.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progress_bar.setVisibility(View.GONE);
                        middle.setVisibility(View.VISIBLE);
                        close.setVisibility(View.VISIBLE);
                        close_main.setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .into(image_);

        dialog.show();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            PROVIDER_LISTING("1");
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void PROVIDER_LISTING(String type) {
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.CATEGORY_ID, type);// 1 for spa ,2= massuer
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.HOME_PROVIDER_LISTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                if (mDialog != null) {
                    try {
                        mDialog.dismiss();
                    } catch (Exception e) {
                    }

                }

                //if (savePref.getID().isEmpty())
                STORE_DEVICE_TOKEN();

                top_list = new ArrayList<>();
                list = new ArrayList<>();
                list_masseur = new ArrayList<>();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject1 = new JSONObject(result);
                        if (jsonmainObject1.getString("code").equalsIgnoreCase("200")) {

                            scrollView.setVisibility(View.VISIBLE);


                            JSONObject jsonmainObject = jsonmainObject1.getJSONObject("body");

                            JSONArray background_providers = jsonmainObject.getJSONArray("background_providers");
                            for (int i = 0; i < background_providers.length(); i++) {
                                JSONObject object = background_providers.getJSONObject(i).getJSONObject("Image");
                                ProviderModel providerModel = new ProviderModel();
                                providerModel.setId(object.getString("type_id"));
                                providerModel.setImage(object.getString("image"));
                                top_list.add(providerModel);
                            }


                            adapter = new HomeViewPagerAdapter(getActivity(), top_list);
                            pager.setAdapter(adapter);
                            indicator.setViewPager(pager);

                            pager.startAutoScroll(2500);


                            JSONArray spa_providers = jsonmainObject.getJSONArray("spa_providers");
                            for (int i = 0; i < spa_providers.length(); i++) {
                                JSONObject object = spa_providers.getJSONObject(i).getJSONObject("Provider");
                                ProviderModel providerModel = new ProviderModel();
                                providerModel.setId(object.getString("id"));
                                providerModel.setFirst_name(object.getString("first_name"));
                                providerModel.setView(object.getString("view"));
                                providerModel.setIs_open(object.getString("is_open"));
                                if (object.getString("category_name").equals("Spa")) {
                                    providerModel.setId_show("S0" + object.getString("id_show"));
                                } else {
                                    providerModel.setId_show("M0" + object.getString("id_show"));
                                }
                                providerModel.setReview(object.getString("review"));
                                providerModel.setRating(object.getString("rating"));
                                if (spa_providers.getJSONObject(i).getJSONArray("Image").length() > 0)
                                    providerModel.setImage(spa_providers.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                list.add(providerModel);
                            }
                            myRecyclerViewLatestMasseur.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            myRecyclerViewLatestMasseur.setAdapter(new AddListingHomeAdapter(context, list, "1"));

                            // savePref.setStringLatest("last_pos", String.valueOf(position));

                            if (!savePref.getStringLatest("provider_type").isEmpty()) {
                                if (savePref.getStringLatest("provider_type").equals("1")) {
                                    if (!savePref.getStringLatest("last_pos").isEmpty()) {
                                        myRecyclerViewLatestMasseur.scrollToPosition(Integer.parseInt(savePref.getStringLatest("last_pos")));
                                        savePref.setStringLatest("last_pos", "");
                                        savePref.setStringLatest("provider_type", "");
                                    }
                                }
                            }


                            JSONArray masseur_providers = jsonmainObject.getJSONArray("masseur_providers");
                            for (int i = 0; i < masseur_providers.length(); i++) {
                                JSONObject object = masseur_providers.getJSONObject(i).getJSONObject("Provider");
                                ProviderModel providerModel = new ProviderModel();
                                providerModel.setId(object.getString("id"));
                                providerModel.setFirst_name(object.getString("first_name"));
                                providerModel.setView(object.getString("view"));
                                providerModel.setIs_open(object.getString("is_open"));
                                if (object.getString("category_name").equals("Spa")) {
                                    providerModel.setId_show("S0" + object.getString("id_show"));
                                } else {
                                    providerModel.setId_show("M0" + object.getString("id_show"));
                                }
                                providerModel.setReview(object.getString("review"));
                                providerModel.setRating(object.getString("rating"));
                                if (masseur_providers.getJSONObject(i).getJSONArray("Image").length() > 0)
                                    providerModel.setImage(masseur_providers.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                list_masseur.add(providerModel);
                            }
                            myRecyclerViewLatestSpa.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                            myRecyclerViewLatestSpa.setAdapter(new AddListingHomeAdapter(context, list_masseur, "2"));

                            if (!savePref.getStringLatest("provider_type").isEmpty()) {
                                if (savePref.getStringLatest("provider_type").equals("2")) {
                                    if (!savePref.getStringLatest("last_pos").isEmpty()) {
                                        myRecyclerViewLatestSpa.scrollToPosition(Integer.parseInt(savePref.getStringLatest("last_pos")));
                                        savePref.setStringLatest("last_pos", "");
                                        savePref.setStringLatest("provider_type", "");
                                    }
                                }
                            }


                        } else {
                            if (jsonmainObject1.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject1.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (timer != null)
            timer.cancel();
        unbinder.unbind();
        if (pager != null)
            pager.stopAutoScroll();
    }


    @OnClick(R.id.search_bar)
    public void onClick() {
        Intent intent = new Intent(context, FilterActivity.class);
        intent.putExtra("from_home", true);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

    @OnClick({R.id.spa, R.id.masseur})
    public void onClick(View view) {
        Intent intent = new Intent(context, ServiceProviderListingActivity.class);
        switch (view.getId()) {
            case R.id.spa:
                intent.putExtra("category_id", "1");
                intent.putExtra("category_name", "Spa");
                break;
            case R.id.masseur:
                intent.putExtra("category_id", "2");
                intent.putExtra("category_name", "Masseur");
                break;
        }
        context.startActivity(intent);
        MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

}