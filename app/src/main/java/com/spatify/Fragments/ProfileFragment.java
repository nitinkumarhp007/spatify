package com.spatify.Fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.spatify.Activities.AboutUsActivity;
import com.spatify.Activities.ChangePasswordActivity;
import com.spatify.Activities.ContactUSActivity;
import com.spatify.Activities.PrivacyActivity;
import com.spatify.Activities.SignInActivity;
import com.spatify.Activities.UpdateProfileActivity;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    Context context;
    private SavePref savePref;
    Unbinder unbinder;
    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.edit_profile_text)
    TextView editProfileText;
    @BindView(R.id.edit_profile111)
    RelativeLayout editProfile111;
    @BindView(R.id.change_password)
    TextView changePassword;
    @BindView(R.id.change_password___)
    RelativeLayout changePassword___;
    @BindView(R.id.about_us)
    TextView aboutUs;
    @BindView(R.id.about_us_layout)
    RelativeLayout aboutUsLayout;
    @BindView(R.id.terms)
    TextView terms;
    @BindView(R.id.terms_layout)
    RelativeLayout termsLayout;
    @BindView(R.id.logout)
    TextView logout;
    @BindView(R.id.logout_layout)
    RelativeLayout logoutLayout;
    @BindView(R.id.contact_us)
    TextView contactUs;
    @BindView(R.id.contact_us___)
    RelativeLayout contactUs___;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        Glide.with(context).load(savePref.getImage()).error(R.drawable.user1).into(profilePic);
        // Glide.with(context).load("https://theheavenbankbook.com/uploads/1593665928988.jpeg").error(R.drawable.user1).into(profilePic);
        name.setText(savePref.getName());
        email.setText(savePref.getEmail());
        phone.setText(savePref.getPhone());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.contact_us, R.id.contact_us___, R.id.edit_profile_text, R.id.edit_profile111, R.id.change_password, R.id.change_password___, R.id.about_us, R.id.about_us_layout, R.id.terms, R.id.terms_layout, R.id.logout, R.id.logout_layout})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.contact_us:
                startActivity(new Intent(getActivity(), ContactUSActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.contact_us___:
                startActivity(new Intent(getActivity(), ContactUSActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile_text:
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.edit_profile111:
                startActivity(new Intent(getActivity(), UpdateProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.change_password:
                if (savePref.getStringLatest("is_from_social").equals("1")) {
                    util.IOSDialog(context, "Feature not available when user login from google/facebook.");
                } else {
                    startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }

                break;
            case R.id.change_password___:
                if (savePref.getStringLatest("is_from_social").equals("1")) {
                    util.IOSDialog(context, "Feature not available when user login from google/facebook.");
                } else {
                    startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
                    getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                break;
            case R.id.about_us:
                Intent intent = new Intent(getActivity(), PrivacyActivity.class);
                intent.putExtra("privacy",true);
                intent.putExtra("link","https://spatify.sg/privacypolicy/");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.about_us_layout:
                Intent intent1 = new Intent(getActivity(), PrivacyActivity.class);
                intent1.putExtra("privacy",true);
                intent1.putExtra("link","https://spatify.sg/privacypolicy/");
                startActivity(intent1);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.terms:
                Intent intent3 = new Intent(getActivity(), AboutUsActivity.class);
                intent3.putExtra("privacy",false);
                intent3.putExtra("link","https://spatify.sg/termsofuse/");
                startActivity(intent3);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.terms_layout:
                Intent intent5 = new Intent(getActivity(), AboutUsActivity.class);
                intent5.putExtra("privacy",false);
                intent5.putExtra("link","https://spatify.sg/termsofuse/");
                startActivity(intent5);
                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.logout:
                LogoutAlert();
                break;
            case R.id.logout_layout:
                LogoutAlert();
                break;
        }
    }


    private void LogoutAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        util.showToast(context, "User Logout Successfully!");
                        savePref.clearPreferences();
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}