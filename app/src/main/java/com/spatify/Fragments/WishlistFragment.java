package com.spatify.Fragments;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.spatify.Activities.SignInActivity;
import com.spatify.Adapters.FeaturedlistAdapter;
import com.spatify.Adapters.NotificationAdapter;
import com.spatify.Adapters.WishlistAdapter;
import com.spatify.DataModel.ProviderModel;
import com.spatify.R;
import com.spatify.Util.ConnectivityReceiver;
import com.spatify.Util.Parameters;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;
import com.spatify.parser.AllAPIS;
import com.spatify.parser.GetAsync;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class WishlistFragment extends Fragment {
    Context context;
    Unbinder unbinder;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.error_text)
    TextView errorText;
    WishlistAdapter adapter;
    ArrayList<ProviderModel> list;
    private SavePref savePref;

    public WishlistFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        savePref = new SavePref(context);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected())
            FAV_PROVIDER_LISTING();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }

    private void FAV_PROVIDER_LISTING() {
        myRecyclerView.setVisibility(View.GONE);
        ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.USER_ID, savePref.getID());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.FAV_PROVIDER_LISTING, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                list = new ArrayList<>();
                if (list.size() > 0)
                    list.clear();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            JSONArray body = jsonmainObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject object = body.getJSONObject(i).getJSONObject("Provider");
                                ProviderModel providerModel = new ProviderModel();
                                providerModel.setId(object.getString("id"));
                                if (object.getString("category_name").equals("Spa")) {
                                    providerModel.setId_show("S0" + object.getString("id_show"));
                                } else {
                                    providerModel.setId_show("M0" + object.getString("id_show"));
                                }
                                providerModel.setFirst_name(object.getString("first_name"));
                                providerModel.setView(object.getString("view"));
                                providerModel.setReview(object.getString("review"));
                                providerModel.setRating(object.getString("rating"));
                                providerModel.setIs_open(object.getString("is_open"));
                                providerModel.setIs_like(object.optString("is_like"));
                                if (body.getJSONObject(i).getJSONArray("Image").length() > 0)
                                    providerModel.setImage(body.getJSONObject(i).getJSONArray("Image").getJSONObject(0).getString("image"));
                                list.add(providerModel);
                            }

                            if (list.size() > 0) {
                                adapter = new WishlistAdapter(context, list, WishlistFragment.this);
                                myRecyclerView.setLayoutManager(new GridLayoutManager(context, 2));
                                myRecyclerView.setAdapter(adapter);


                                // savePref.setStringLatest("last_pos", String.valueOf(position));

                                if (!savePref.getStringLatest("last_pos").isEmpty()) {
                                    myRecyclerView.scrollToPosition(Integer.parseInt(savePref.getStringLatest("last_pos")));
                                    savePref.setStringLatest("last_pos", "");
                                }

                                myRecyclerView.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                myRecyclerView.setVisibility(View.GONE);
                                errorText.setVisibility(View.VISIBLE);
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                savePref.clearPreferences();
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void DO_LIKE_API(int position) {
        util.hideKeyboard(getActivity());
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.PROVIDER_ID, list.get(position).getId());
        formBuilder.addFormDataPart(Parameters.STATUS, "0");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.DO_LIKE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        if (jsonObject.getString("code").equalsIgnoreCase("200")) {
                            list.remove(position);
                            adapter.notifyDataSetChanged();
                        } else {
                            util.IOSDialog(context, jsonObject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}