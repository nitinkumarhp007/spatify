package com.spatify.Fragments;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;
import com.spatify.R;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    String[] lat = new String[5];
    String[] lng = new String[5];
    SupportMapFragment mapFragment;

    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);

        mapFragment = (SupportMapFragment)
                getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lng[0] = "76.41";
        lng[1] = "76.75";
        lng[2] = "76.41";
        lng[3] = "76.41";
        lng[4] = "76.75";

        lat[0] = "34.41";
        lat[1] = "34.75";
        lat[2] = "34.41";
        lat[3] = "34.41";
        lat[4] = "34.75";


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        IconGenerator iconFactory = new IconGenerator(getActivity());
        HashMap<LatLng, Integer> hashMap = new HashMap<>(5);
        for (int i = 0; i < lat.length; i++) {
            LatLng latLng = new LatLng(Double.parseDouble(lat[i]),
                    Double.parseDouble(lng[i]));
            if (i == 0)
                hashMap.put(latLng, 1);
            else {
                if (hashMap.containsKey(latLng)) {
                    int valu = hashMap.get(latLng) + 1;
                    hashMap.put(latLng, valu);
                } else {
                    hashMap.put(latLng, 1);
                }
            }

        }

        for (int i = 0; i < lat.length; i++) {
            LatLng latLng = new LatLng(Double.parseDouble(lat[i]),
                    Double.parseDouble(lng[i]));

            TextView text = new TextView(getActivity());
            text.setText(hashMap.get(latLng).toString());
            text.setTextColor(ContextCompat.getColor(getActivity(),R.color.black));
            IconGenerator generator = new IconGenerator(getActivity());
            generator.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.spatify_marker));
            generator.setContentView(text);
            Bitmap icon = generator.makeIcon();
            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.fromBitmap(icon)));

//            marker.showInfoWindow();
//            marker.setTag(latLng);
        }
//        mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter(getActivity(),hashMap));
        LatLng latLng = new LatLng(Double.parseDouble(lat[0]),
                Double.parseDouble(lng[0]));

        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                latLng, 7);
        mMap.animateCamera(location);
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return false;
            }
        });

//        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//        mMap.setMaxZoomPreference(14.0f);
    }


}
