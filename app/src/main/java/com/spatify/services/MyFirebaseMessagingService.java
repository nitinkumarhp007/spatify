package com.spatify.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.spatify.Activities.AddDetailActivity;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    SavePref savePref;
    private static int i;
    private static int value = 0;

    String message = "", provider_id = "", image = "", package_id = "", title = "", offer_amount = "", notification_code = "", product_id = "", username = "", sender_id = "", chat_message, timeStamp;

    String CHANNEL_ID = "";// The id of the channel.
    String CHANNEL_ONE_NAME = "Channel One";
    NotificationChannel notificationChannel;
    public static NotificationManager notificationManager;
    public static Notification.InboxStyle inboxStyle = new Notification.InboxStyle();
    Notification notification;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject obj = null;
        savePref = new SavePref(getApplicationContext());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getData());


        message = remoteMessage.getData().get("message");
        notification_code = remoteMessage.getData().get("notification_code");

        getManager();
        CHANNEL_ID = getApplicationContext().getPackageName();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ONE_NAME, notificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        }

        if (notification_code.equals("456332")) {//chat
            try {
                JSONObject object = new JSONObject(remoteMessage.getData().get("body"));

                provider_id = object.getString("provider_id");
                package_id = object.getString("package_id");
                image = object.optString("image");
                message = object.getString("description");
                title = object.getString("title");
                sendNotification(getApplicationContext(), message);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            //sendNotification(getApplicationContext(), message);
        }


    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        SavePref.setDeviceToken(getApplicationContext(), "token", token);
    }

    private void sendNotification(Context context, String message) {
        Intent intent = null;
        PendingIntent pendingIntent;

        if (notification_code.equals("456332")) {
            if (!provider_id.equals("0")) {
                intent = new Intent(context, AddDetailActivity.class);
                intent.putExtra("image", image);
                intent.putExtra("id", provider_id);
                intent.putExtra("package_id", package_id);
                //simple text & provider , simple text , image & provider
            } else {
                intent = new Intent(context, MainActivity.class);
                intent.putExtra("image", image);
                intent.putExtra("message", message);

                //simple text , simple text & image
            }


        } else {
            intent = new Intent(context, MainActivity.class);
            intent.putExtra("notification_code", notification_code);
        }
        intent.putExtra("is_from_push", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap image_bitmap = null;
        try {
            URL url = new URL(image);
            image_bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            System.out.println(e);
        }

        Bitmap icon1 = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        /*Notification.Builder notificationBuilder = new Notification.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(image_bitmap)
                .setContentTitle(title)
                .setOngoing(false)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);*/

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher).setLargeIcon(image_bitmap)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notification = notificationBuilder.build();

        notificationManager.notify(i++, notification);


    }

    private NotificationManager getManager() {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return notificationManager;
    }


    // offer_status
    private void publishResultsMessage(String message, String created, String voice_length, String username, String message_id, String message_type) {

        Intent intent = new Intent(util.NOTIFICATION_MESSAGE);
        intent.putExtra("message", message);
        intent.putExtra("created", created);
        intent.putExtra("username", username);
        intent.putExtra("message_id", message_id);
        intent.putExtra("voice_length", voice_length);
        intent.putExtra("message_type", message_type);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
