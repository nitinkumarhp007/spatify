package com.spatify.DataModel;

import android.os.Parcel;
import android.os.Parcelable;

public class ProviderModel implements Parcelable {
    String id = "";
    String id_show = "";
    String first_name = "";
    String image = "";
    String view = "";
    String review = "";
    String rating = "";
    String is_like = "";
    String lat = "";
    String lng = "";
    String is_open = "";
    String is_selected = "";
    String current_count = "";
    String count = "";
    String address = "";
    boolean is_changed = false;

    public ProviderModel() {
    }


    protected ProviderModel(Parcel in) {
        id = in.readString();
        id_show = in.readString();
        first_name = in.readString();
        image = in.readString();
        view = in.readString();
        review = in.readString();
        rating = in.readString();
        is_like = in.readString();
        lat = in.readString();
        lng = in.readString();
        is_open = in.readString();
        is_selected = in.readString();
        current_count = in.readString();
        count = in.readString();
        address = in.readString();
        is_changed = in.readBoolean();
    }

    public static final Creator<ProviderModel> CREATOR = new Creator<ProviderModel>() {
        @Override
        public ProviderModel createFromParcel(Parcel in) {
            return new ProviderModel(in);
        }

        @Override
        public ProviderModel[] newArray(int size) {
            return new ProviderModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getIs_like() {
        return is_like;
    }

    public boolean isIs_changed() {
        return is_changed;
    }

    public void setIs_changed(boolean is_changed) {
        this.is_changed = is_changed;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getId_show() {
        return id_show;
    }

    public void setId_show(String id_show) {
        this.id_show = id_show;
    }

    public String getIs_open() {
        return is_open;
    }

    public void setIs_open(String is_open) {
        this.is_open = is_open;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    public String getReview() {
        return review;
    }

    public String getIs_selected() {
        return is_selected;
    }

    public void setIs_selected(String is_selected) {
        this.is_selected = is_selected;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getCurrent_count() {
        return current_count;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setCurrent_count(String current_count) {
        this.current_count = current_count;
    }

    public String getRating() {
        return rating;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(id_show);
        dest.writeString(first_name);
        dest.writeString(image);
        dest.writeString(view);
        dest.writeString(review);
        dest.writeString(rating);
        dest.writeString(is_like);
        dest.writeString(lat);
        dest.writeString(lng);
        dest.writeString(is_open);
        dest.writeString(is_selected);
        dest.writeString(current_count);
        dest.writeString(count);
        dest.writeString(address);
        dest.writeBoolean(is_changed);
    }
}
