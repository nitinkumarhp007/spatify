package com.spatify.DataModel;

import android.os.Parcel;
import android.os.Parcelable;

public class FacilityServiceModel implements Parcelable {
    String id="";
    String name="";
    boolean is_select=false;

    public FacilityServiceModel()
    {}

    protected FacilityServiceModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        is_select = in.readByte() != 0;
    }

    public static final Creator<FacilityServiceModel> CREATOR = new Creator<FacilityServiceModel>() {
        @Override
        public FacilityServiceModel createFromParcel(Parcel in) {
            return new FacilityServiceModel(in);
        }

        @Override
        public FacilityServiceModel[] newArray(int size) {
            return new FacilityServiceModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public boolean isIs_select() {
        return is_select;
    }

    public void setIs_select(boolean is_select) {
        this.is_select = is_select;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeByte((byte) (is_select ? 1 : 0));
    }
}

