package com.spatify.DataModel;

import com.google.android.gms.maps.model.LatLng;

public class MapModel {

    public MapModel( int total, int count) {
        this.total = total;
        this.count = count;
    }

    int total;
    int count;


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
