package com.spatify.DataModel;

public class AreaModel {

    public AreaModel(String name, boolean is_check) {
        this.name = name;
        this.is_check = is_check;
    }

    String name="";
    boolean is_check=false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIs_check() {
        return is_check;
    }

    public void setIs_check(boolean is_check) {
        this.is_check = is_check;
    }
}
