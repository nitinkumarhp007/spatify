package com.spatify.parser;


public class AllAPIS {

    public static final String BASE_URL = "http://www.app.spatify.sg/spatify/";
//    public static final String BASE_URL = "http://3.22.158.181/spatify/";

    public static final String USERLOGIN = BASE_URL + "apis/UserLogin";
    public static final String USER_SIGNUP = BASE_URL + "apis/user_signup";
    public static final String VERIFY_OTP = BASE_URL + "apis/VerifyOtp";
    public static final String RESEND_OTP = BASE_URL + "apis/resend_otp";
    public static final String SOCIAL_LOGIN = BASE_URL + "users/soical_login";
    public static final String FORGOT_PASSWORD = BASE_URL + "apis/forgot_password";
    public static final String LOGOUT = BASE_URL + "apis/logout";
    public static final String CHANGEPASSWORD = BASE_URL + "apis/changePassword";
    public static final String EDIT_PROFILE = BASE_URL + "apis/edit_profile";
    public static final String STORE_DEVICE_TOKEN = BASE_URL + "apis/store_device_token";
    public static final String APP_INFO = BASE_URL + "app-information";

    public static final String HOME_PROVIDER_LISTING = BASE_URL + "apis/home_provider_listing";
    public static final String FEATURED_PROVIDER_LISTING = BASE_URL + "apis/featured_provider_listing";
    public static final String FAV_PROVIDER_LISTING = BASE_URL + "apis/wishlist";
    public static final String FILTER_PROVIDER_LISTING = BASE_URL + "apis/filter_provider_listing";
    public static final String PROVIDER_DETAIL = BASE_URL + "apis/provider_detail";
    public static final String DO_LIKE = BASE_URL + "apis/do_like";
    public static final String RATE_PROVIDER = BASE_URL + "apis/rate_provider";
    public static final String PROVIDER_RATINGS = BASE_URL + "apis/provider_ratings";
    public static final String ALL_ATTRIBUTES = BASE_URL + "apis/all_attributes";
    public static final String SMS_AND_CALL_COUNT = BASE_URL + "apis/sms_and_call_count";
    public static final String CONTACT_US = BASE_URL + "apis/contact_us";


}
