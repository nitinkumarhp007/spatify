package com.spatify.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.spatify.Activities.AddDetailActivity;
import com.spatify.DataModel.ProviderModel;
import com.spatify.MainActivity;
import com.spatify.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ServiceFacilitiesAdapter extends RecyclerView.Adapter<ServiceFacilitiesAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;


    private View view;
    ArrayList<String> list;

    public ServiceFacilitiesAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.service_facility_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        String s = list.get(position);

        if (s.contains("@")) {
            String f = s.substring(0, s.lastIndexOf("@"));
            String l = s.substring(s.lastIndexOf("@"));

            holder.name.setText(f + "\n" + l.substring(1));
        } else {
            holder.name.setText(list.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
