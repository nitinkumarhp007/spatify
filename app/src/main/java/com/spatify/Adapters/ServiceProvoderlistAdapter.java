package com.spatify.Adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.spatify.Activities.AddDetailActivity;
import com.spatify.Activities.ServiceProviderListingActivity;
import com.spatify.DataModel.ProviderModel;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ServiceProvoderlistAdapter extends RecyclerView.Adapter<ServiceProvoderlistAdapter.RecyclerViewHolder> {
    ServiceProviderListingActivity context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ProviderModel> list;
    private SavePref savePref;

    public ServiceProvoderlistAdapter(ServiceProviderListingActivity context, ArrayList<ProviderModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);
        savePref = new SavePref(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.service_provider_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        Glide.with(context).load(list.get(position).getImage()).into(holder.image);

        holder.name.setText(list.get(position).getFirst_name());

        holder.id_.setText("ID:" + list.get(position).getId_show());
        holder.view_.setText(list.get(position).getView());
        holder.review_.setText(list.get(position).getReview());
        holder.review__.setText(list.get(position).getReview());
        holder.ratingBar.setRating(Float.parseFloat(list.get(position).getRating()));

        if (list.get(position).getIs_like().equals("1")) {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_wf));
        } else {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_w));
        }
        if (list.get(position).getIs_open().equals("1")) {
            holder.openNow.setVisibility(View.VISIBLE);
            holder.openNow.setText("Open Now");
            holder.openNow.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.openNow.setVisibility(View.VISIBLE);
            holder.openNow.setText("Close Now");
            holder.openNow.setTextColor(context.getResources().getColor(R.color.red));
        }

        holder.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (savePref.getAuthorization_key().isEmpty()) {
                    util.login_first(context);
                } else {
                    context.DO_LIKE_API(position);
                }


            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePref.setStringLatest("last_pos", String.valueOf(position));
                Intent intent = new Intent(context, AddDetailActivity.class);
                intent.putExtra("id", list.get(position).getId());
                intent.putExtra("is_filter", context.is_filter);
                context.startActivityForResult(intent, 400);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        RoundedImageView image;
        @BindView(R.id.id_)
        TextView id_;
        @BindView(R.id.view_)
        TextView view_;
        @BindView(R.id.review_)
        TextView review_;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.heart)
        ImageView heart;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.review__)
        TextView review__;
        @BindView(R.id.rating_bar)
        RatingBar ratingBar;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.open_now)
        TextView openNow;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
