package com.spatify.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.spatify.R;
import com.spatify.Util.util;

import net.igenius.customcheckbox.CustomCheckBox;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AreaAdapter extends RecyclerView.Adapter<AreaAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    String[] list;
    boolean[] list_check;
    private View view;

    public AreaAdapter(Context context, String[] list, boolean[] list_check) {
        this.context = context;
        this.list = list;
        this.list_check = list_check;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.area_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        if (util.list_check[position]) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }


        holder.name.setText(list[position].trim());

        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                util.list_check[position] = isChecked;
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.checkbox)
        com.rey.material.widget.CheckBox checkbox;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
