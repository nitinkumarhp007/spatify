package com.spatify.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.spatify.Activities.AddDetailActivity;
import com.spatify.Activities.ImageShowActivity;
import com.spatify.Activities.PhotosActivity;
import com.spatify.DataModel.ProviderModel;
import com.spatify.MainActivity;
import com.spatify.R;

import java.util.ArrayList;

public class HomeViewPagerAdapter extends PagerAdapter {
    int[] img;
    LayoutInflater inflater;
    Context context;
    ArrayList<ProviderModel> top_list;

    public HomeViewPagerAdapter(Context addDetailActivity, ArrayList<ProviderModel> top_list) {
        this.context = addDetailActivity;
        this.top_list = top_list;

    }

    @Override
    public int getCount() {
        return top_list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView img;
        final ProgressBar progress_bar;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemview = inflater.inflate(R.layout.home_viewpager_item, container, false);


        itemview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (top_list.get(position).getId().equals("0")) {
                    Intent intent = new Intent(context, ImageShowActivity.class);
                    intent.putExtra("image", top_list.get(position).getImage());
                    context.startActivity(intent);
                    MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                } else {
                    Intent intent = new Intent(context, AddDetailActivity.class);
                    intent.putExtra("id", top_list.get(position).getId());
                    context.startActivity(intent);
                    MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }

            }
        });

        img = (ImageView) itemview.findViewById(R.id.ima1);
        progress_bar = (ProgressBar) itemview.findViewById(R.id.progress_bar);
        progress_bar.setVisibility(View.GONE);
        Glide.with(context)
                .load(top_list.get(position).getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progress_bar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progress_bar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(img);


        //add item.xml to viewpager
        ((ViewPager) container).addView(itemview);
        return itemview;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}