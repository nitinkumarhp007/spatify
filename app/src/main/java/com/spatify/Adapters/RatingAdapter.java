package com.spatify.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.spatify.DataModel.ReviewModel;
import com.spatify.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class RatingAdapter extends RecyclerView.Adapter<RatingAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;
    ArrayList<ReviewModel> list;

    public RatingAdapter(Context context, ArrayList<ReviewModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.rating_item, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        String timestamp = list.get(position).getCreated(); //timestamp : 1254155422
        long timestampString = Long.parseLong(timestamp);
        String value = new SimpleDateFormat("dd/MM/yyyy").
                format(new Date(timestampString * 1000)); //convertion to 16/05/2017 17:33:42

        // holder.time.setText(value);
        Log.e("value", value);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = (Date) formatter.parse(value);//value:  16/05/2017 17:33:42
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //String date_text = date.toString().substring(0, 10);


        //Date date = new Date();
        String stringDate = DateFormat.getDateTimeInstance().format(date);

        holder.date.setText(stringDate.substring(0, 12));


        Glide.with(context).load(list.get(position).getImage()).error(R.drawable.user1).into(holder.image);
        holder.name.setText(list.get(position).getName());
        holder.text.setText(list.get(position).getText());
        holder.ratingBar.setRating(Float.parseFloat(list.get(position).getRating()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.rating_bar)
        RatingBar ratingBar;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.date)
        TextView date;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
