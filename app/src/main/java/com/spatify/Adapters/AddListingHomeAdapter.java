package com.spatify.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.spatify.Activities.AddDetailActivity;
import com.spatify.DataModel.ProviderModel;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.SavePref;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddListingHomeAdapter extends RecyclerView.Adapter<AddListingHomeAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    String s;
    private View view;
    ArrayList<ProviderModel> list;

    public AddListingHomeAdapter(Context context, ArrayList<ProviderModel> list, String s) {
        this.context = context;
        this.list = list;
        this.s = s;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.add_home_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Glide.with(context).load(list.get(position).getImage()).into(holder.image);

        holder.name.setText(list.get(position).getFirst_name());

        holder.id_.setText("ID:" + list.get(position).getId_show());
        holder.view_.setText(list.get(position).getView());
        holder.review_.setText(list.get(position).getReview());

        if (list.get(position).getIs_open().equals("1")) {
            holder.openNow.setVisibility(View.VISIBLE);
            holder.openNow.setText("Open Now");
            holder.openNow.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.openNow.setVisibility(View.VISIBLE);
            holder.openNow.setText("Close Now");
            holder.openNow.setTextColor(context.getResources().getColor(R.color.red));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SavePref savePref = new SavePref(context);
                savePref.setStringLatest("last_pos", String.valueOf(position));
                savePref.setStringLatest("provider_type", s);

                Intent intent = new Intent(context, AddDetailActivity.class);
                intent.putExtra("id", list.get(position).getId());
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        RoundedImageView image;
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.id_)
        TextView id_;
        @BindView(R.id.view__)
        TextView view_;
        @BindView(R.id.review___)
        TextView review_;
        @BindView(R.id.open_now)
        TextView openNow;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
