package com.spatify.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.spatify.Fragments.HomeFragment;
import com.spatify.Fragments.SpaFeaturedFragment;

public class MyAdapter extends FragmentStatePagerAdapter {

    private Context myContext;
    int totalTabs;

    public MyAdapter(Context context, FragmentManager fm, int totalTabs) {
        super(fm);
        myContext = context;
        this.totalTabs = totalTabs;
    }

    // this is for fragment tabs
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                SpaFeaturedFragment homeFragment = new SpaFeaturedFragment("1");
                return homeFragment;
            case 1:
                SpaFeaturedFragment sportFragment = new SpaFeaturedFragment("2");
                return sportFragment;
            default:
                return null;
        }
    }

    // this counts total number of tabs
    @Override
    public int getCount() {
        return totalTabs;
    }
}

