package com.spatify.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.spatify.Activities.CountryListActivity;
import com.spatify.R;
import com.spatify.Util.util;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.RecyclerViewHolder> {
    CountryListActivity context;
    LayoutInflater Inflater;
    String[] list;
    private View view;

    public CountryAdapter(CountryListActivity context, String[] list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.country_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        holder.name.setText(list[position].trim());

        if (position == 0) {
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.china));
        } else if (position == 1) {
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.singapore));
        } else if (position == 2) {
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.vietnam));
        } else if (position == 3) {
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.thailand));
        } else if (position == 4) {
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.malaysia));
        } else if (position == 5) {
            holder.image.setImageDrawable(context.getResources().getDrawable(R.drawable.myanmar));
        }


        /*{
            "China", "Singapore", "Vietnam", "Thailand", "Malaysia", "Myanmar"
        } ;*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.donetask(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.length;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.image)
        ImageView image;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
