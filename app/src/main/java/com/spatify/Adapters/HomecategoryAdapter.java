package com.spatify.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.makeramen.roundedimageview.RoundedImageView;
import com.spatify.Activities.ServiceProviderListingActivity;
import com.spatify.MainActivity;
import com.spatify.R;
import butterknife.BindView;
import butterknife.ButterKnife;


public class HomecategoryAdapter extends RecyclerView.Adapter<HomecategoryAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    private View view;

    public HomecategoryAdapter(Context context) {
        this.context = context;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.home_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

       /* holder.name.setText(list.get(position).getName());


        Glide.with(context)
                .load(list.get(position).getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, com.bumptech.glide.request.target.Target<Drawable> target, boolean isFirstResource) {
                        holder.progress_bar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, com.bumptech.glide.request.target.Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progress_bar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SubcategoryActivity.class);
                intent.putExtra("category_id", list.get(position).getId());
                intent.putExtra("category_name", list.get(position).getName());
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });
*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ServiceProviderListingActivity.class);
                //intent.putExtra("category_id", list.get(position).getId());
                //intent.putExtra("category_name", list.get(position).getName());
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
            }
        });

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
