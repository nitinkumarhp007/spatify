package com.spatify.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.spatify.DataModel.FacilityServiceModel;
import com.spatify.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FilterServiceFacilitiesAdapter extends RecyclerView.Adapter<FilterServiceFacilitiesAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;


    private View view;
    ArrayList<FacilityServiceModel> list;

    public FilterServiceFacilitiesAdapter(Context context, ArrayList<FacilityServiceModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.service_facility_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {


        String s = list.get(position).getName();

        if (s.contains("@")) {
            String f = s.substring(0, s.lastIndexOf("@"));
            String l = s.substring(s.lastIndexOf("@"));

            holder.name.setText(f + "\n" + l.substring(1));
        } else {
            holder.name.setText(s);
        }


        if (list.get(position).isIs_select()) {
            holder.name.setBackground(context.getResources().getDrawable(R.drawable.drawable_button_sel));
            holder.name.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.name.setBackground(context.getResources().getDrawable(R.drawable.drawable_button));
            holder.name.setTextColor(context.getResources().getColor(R.color.black));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!list.get(position).isIs_select())
                    list.get(position).setIs_select(true);
                else
                    list.get(position).setIs_select(false);

                notifyDataSetChanged();
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
