package com.spatify.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.spatify.Activities.ServiceDetailActivity;
import com.spatify.DataModel.PackageModel;
import com.spatify.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;

    private View view;
    ArrayList<PackageModel> package_list;

    public ServicesAdapter(Context context, ArrayList<PackageModel> package_list) {
        this.context = context;
        this.package_list = package_list;
        Inflater = LayoutInflater.from(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.services_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        String s = package_list.get(position).getName();

        if (s.contains("@")) {
            String f = s.substring(0, s.lastIndexOf("@"));
            String l = s.substring(s.lastIndexOf("@"));

            holder.name.setText(f + "\n" + l.substring(1));
        } else {
            holder.name.setText(s);
        }
        holder.price.setText(package_list.get(position).getPrice());

        if (package_list.get(position).getIs_featured().equals("1")) {
            holder.featured.setVisibility(View.VISIBLE);
        } else {
            holder.featured.setVisibility(View.INVISIBLE);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, ServiceDetailActivity.class);
                intent.putExtra("data",package_list.get(position));
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return package_list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.featured)
        TextView featured;
        @BindView(R.id.topPanel)
        RelativeLayout topPanel;
        @BindView(R.id.price)
        TextView price;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
