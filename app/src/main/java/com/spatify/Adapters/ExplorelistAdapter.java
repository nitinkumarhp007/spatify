package com.spatify.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.spatify.Activities.AddDetailActivity;
import com.spatify.DataModel.ProviderModel;
import com.spatify.Fragments.ExploreFragment;
import com.spatify.MainActivity;
import com.spatify.R;
import com.spatify.Util.SavePref;
import com.spatify.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ExplorelistAdapter extends RecyclerView.Adapter<ExplorelistAdapter.RecyclerViewHolder> {
    Context context;
    LayoutInflater Inflater;
    ExploreFragment exploreFragment;
    private View view;
    SavePref savePref;
    ArrayList<ProviderModel> list;

    public ExplorelistAdapter(Context context, ArrayList<ProviderModel> list, ExploreFragment exploreFragment) {
        this.context = context;
        this.list = list;
        savePref = new SavePref(context);
        this.exploreFragment = exploreFragment;
        Inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.explore_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        Glide.with(context).load(list.get(position).getImage()).into(holder.image);

        holder.text.setText(list.get(position).getFirst_name());

        holder.review.setText(list.get(position).getReview());
        holder.ratingBar.setRating(Float.parseFloat(list.get(position).getRating()));

        if (list.get(position).getIs_like().equals("1")) {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_wf));
        } else {
            holder.heart.setImageDrawable(context.getResources().getDrawable(R.drawable.heart_w));
        }
        holder.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (savePref.getAuthorization_key().isEmpty()) {
                    util.login_first(context);
                } else {
                    exploreFragment.DO_LIKE_API(position);
                }


            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddDetailActivity.class);
                intent.putExtra("id", list.get(position).getId());//133
                context.startActivity(intent);
                MainActivity.context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);


            }
        });
        // exploreFragment.Move_map(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.review)
        TextView review;
        @BindView(R.id.rating_bar)
        RatingBar ratingBar;
        @BindView(R.id.heart)
        ImageView heart;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
