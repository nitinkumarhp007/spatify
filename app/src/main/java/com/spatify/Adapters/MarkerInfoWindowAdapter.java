package com.spatify.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.spatify.R;

import java.util.HashMap;

public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Context context;
    HashMap<LatLng,Integer> hashMap;
    public MarkerInfoWindowAdapter(Context context, HashMap<LatLng,Integer> hashMap) {
        this.context = context.getApplicationContext();
        this.hashMap = hashMap;
    }

    @Override
    public View getInfoWindow(Marker arg0) {
        return null;
    }

    @Override
    public View getInfoContents(Marker arg0) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v =  inflater.inflate(R.layout.map_marker_info_window, null);

        TextView tv_count = (TextView) v.findViewById(R.id.tv_count);
        tv_count.setText(hashMap.get(arg0.getTag()).toString());
        return v;
    }
}